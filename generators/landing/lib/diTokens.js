"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("@airport/di");
exports.SCHEMA_BUILDER = di_1.diToken();
exports.SCHEMA_CHECKER = di_1.diToken();
exports.SCHEMA_COMPOSER = di_1.diToken();
exports.SCHEMA_INITIALIZER = di_1.diToken();
exports.SCHEMA_LOCATOR = di_1.diToken();
exports.SCHEMA_RECORDER = di_1.diToken();
//# sourceMappingURL=diTokens.js.map
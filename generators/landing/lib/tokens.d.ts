import { ISchemaBuilder } from './builder/ISchemaBuilder';
import { ISchemaChecker } from './checker/SchemaChecker';
import { ISchemaLocator } from './locator/SchemaLocator';
import { ISchemaComposer } from './recorder/SchemaComposer';
import { ISchemaRecorder } from './recorder/SchemaRecorder';
import { ISchemaInitializer } from './SchemaInitializer';
export declare const SCHEMA_BUILDER: import("@airport/di").IDiToken<ISchemaBuilder>;
export declare const SCHEMA_CHECKER: import("@airport/di").IDiToken<ISchemaChecker>;
export declare const SCHEMA_COMPOSER: import("@airport/di").IDiToken<ISchemaComposer>;
export declare const SCHEMA_INITIALIZER: import("@airport/di").IDiToken<ISchemaInitializer>;
export declare const SCHEMA_LOCATOR: import("@airport/di").IDiToken<ISchemaLocator>;
export declare const SCHEMA_RECORDER: import("@airport/di").IDiToken<ISchemaRecorder>;
//# sourceMappingURL=tokens.d.ts.map
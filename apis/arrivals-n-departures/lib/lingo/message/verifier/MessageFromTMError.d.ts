export declare enum MessageFromTMError {
    NO_DATA_ERROR = 0,
    MESSAGE_IS_NOT_ARRAY = 1,
    PROTOCOL_VERSION_NOT_SUPPORTED = 2,
    MESSAGE_TYPE_IS_NOT_NUMBER = 3,
    WRONG_MESSAGE_LENGTH = 4,
    WRONG_CONTENT_TYPE = 5,
    UNSUPPORTED_CONTENT_TYPE = 6,
    DATABASE_INFO_IS_NOT_ARRAY = 7,
    WRONG_DATABASE_INFO_LENGTH = 8,
    TERMINAL_ID_TYPE_IS_NOT_NUMBER = 9,
    TERMINAL_ID_TYPE_IS_INVALID_NUMBER = 10,
    TERMINAL_PASSWORD_TYPE_IS_NOT_STRING = 11,
    WRONG_AGT_TERMINAL_PASSWORD_LENGTH = 12,
    TM_SHARING_MESSAGE_ID_IS_NOT_NUMBER = 13,
    TM_SHARING_MESSAGE_ID_IS_INVALID_NUMBER = 14,
    REPOSITORY_UPDATE_REQUESTS_IS_NOT_ARRAY = 15,
    A_REPOSITORY_UPDATE_REQUEST_IS_NOT_ARRAY = 16,
    WRONG_REPOSITORY_UPDATE_REQUEST_LENGTH = 17,
    AGT_REPOSITORY_ID_IS_NOT_NUMBER = 18,
    AGT_REPOSITORY_ID_IS_INVALID_NUMBER = 19,
    TM_REPOSITORY_TRANSACTION_BLOCK_ID_IS_NOT_NUMBER = 20,
    AGT_REPOSITORY_TRANSACTION_BLOCK_CONTENTS_NOT_STRING = 21,
    AGT_REPOSITORY_TRANSACTION_BLOCK_CONTENTS_TOO_LONG = 22,
    SUM_OF_REPOSITORY_TRANSACTION_BLOCK_CONTENTS_IS_TOO_LONG = 23,
    AGT_SHARING_MESSAGE_IDS_IS_NOT_ARRAY = 24,
    AGT_SHARING_MESSAGE_ID_IS_NOT_NUMBER = 25,
    AGT_SHARING_MESSAGE_ID_IS_INVALID_NUMBER = 26,
    SERIALIZED_SYNCS_TO_VERIFY_IS_NOT_ARRAY = 27,
    SERIALIZED_SYNCS_TO_VERIFY_LENGTH_IS_NOT_1 = 28,
    SHARING_MESSAGES_TO_VERIFY_IS_NOT_ARRAY = 29,
    SHARING_MESSAGE_ID_TO_VERIFY_IS_NOT_NUMBER = 30,
    SHARING_MESSAGE_ID_TO_VERIFY_IS_INVALID_NUMBER = 31,
    SHARING_MESSAGES_TO_VERIFY_NOT_SUPPORTED = 32,
    REPO_TRANS_BLOCKS_TO_VERIFY_IS_NOT_ARRAY = 33,
    REPO_TRANS_BLOCKS_TO_VERIFY_NOT_SUPPORTED = 34
}
//# sourceMappingURL=MessageFromTMError.d.ts.map
export declare type TerminalId = number;
export declare type TerminalName = string;
export declare type TerminalPassword = string;
export declare type TerminalSecondId = number;
export declare type AgtSharingMessageId = number;
export declare type AgtRepositoryId = number;
export declare type RepositoryTransactionBlockContents = string;
export declare type RepositoryTransactionBlockData = string;
export declare type TmRepositoryTransactionBlockId = number;
export declare type TmSharingMessageId = number;
//# sourceMappingURL=CoreAgtTypes.d.ts.map
import { diToken } from '@airport/di';
export const MESSAGE_FROM_TM_DESERIALIZER = diToken();
export const MESSAGE_FROM_TM_SERIALIZER = diToken();
export const MESSAGE_FROM_TM_VERIFIER = diToken();
export const MESSAGE_TO_TM_DESERIALIZER = diToken();
export const MESSAGE_TO_TM_SERIALIZER = diToken();
export const MESSAGE_TO_TM_VERIFIER = diToken();
export const SYNC_CONNECTION_SERVER = diToken();
export const TM_DATA_SERIALIZER = diToken();
export const TM_DATA_DESERIALIZER = diToken();
export const TM_DATA_FORMAT_VERIFIER = diToken();
export const TM_DATA_SCHEMA_VERIFIER = diToken();
//# sourceMappingURL=diTokens.js.map
export enum MessageToTMError {
	NO_DATA_ERROR,
	MESSAGE_IS_NOT_ARRAY,
	PROTOCOL_VERSION_NOT_SUPPORTED,
	MESSAGE_TYPE_IS_NOT_NUMBER,
	MESSAGES_BATCH_IS_NOT_ARRAY,
	WRONG_MESSAGES_BATCH_LENGTH,
	TARGET_AGT_DATABASE_IDS_IS_NOT_ARRAY,
	TARGET_AGT_DATABASE_ID_IS_NOT_NUMBER,
	TARGET_AGT_DATABASE_ID_IS_INVALID_NUMBER,
	AGT_DATABASE_SYNC_LOG_ID_IS_NOT_NUMBER,
	AGT_DATABASE_SYNC_LOG_ID_IS_INVALID_NUMBER,
	MESSAGES_IS_NOT_ARRAY,
	// NO_MESSAGES_IN_ARRAY, // No messages is OK
	WRONG_CONTENT_TYPE,
	UNSUPPORTED_CONTENT_TYPE,
	TM_SHARING_MESSAGE_ID_IS_NOT_NUMBER,
	AGT_SYNC_RECORD_ADD_DATETIME_IS_NOT_NUMBER,
	TM_REPOSITORY_TRANSACTION_BLOCK_ID_IS_NOT_NUMBER,
	AGT_SYNC_RECORD_ID_IS_NOT_NUMBER,
	WRONG_REPO_TRANS_BLOCK_SYNC_OUTCOME_TYPE,
	AGT_DATABASE_ID_IS_NOT_NUMBER,
	AGT_REPOSITORY_ID_IS_NOT_NUMBER,
	AGT_SYNC_RECORD_REPOSITORY_TRANSACTION_BLOCK_IS_NOT_STRING,
}
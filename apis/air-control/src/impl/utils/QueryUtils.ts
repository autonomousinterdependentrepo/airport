import {DI}                    from '@airport/di'
import {
	JSONBaseOperation,
	JSONValueOperation,
	OperationCategory,
	SqlOperator
}                              from '@airport/ground-control'
import {QUERY_UTILS}           from '../../tokens'
import {IFieldColumnAliases}   from '../../lingo/core/entity/Aliases'
import {JSONLogicalOperation}  from '../../lingo/core/operation/LogicalOperation'
import {JSONRawValueOperation} from '../../lingo/core/operation/Operation'
import {RawFieldQuery}         from '../../lingo/query/facade/FieldQuery'
import {IFieldUtils}           from '../../lingo/utils/FieldUtils'
import {IQueryUtils}           from '../../lingo/utils/QueryUtils'
import {QExistsFunction}       from '../core/field/Functions'
import {QOperableField}        from '../core/field/OperableField'
import {wrapPrimitive}         from '../core/field/WrapperFunctions'
import {TreeQuery}             from '../query/facade/TreeQuery'

declare function require(moduleName: string): any;

export class QueryUtils
	implements IQueryUtils {

	whereClauseToJSON(
		whereClause: JSONBaseOperation,
		columnAliases: IFieldColumnAliases<any>,
		fieldUtils: IFieldUtils
	): JSONBaseOperation {
		if (!whereClause) {
			return null
		}
		let operation: JSONBaseOperation     = whereClause
		let jsonOperation: JSONBaseOperation = {
			c: operation.c,
			o: operation.o
		}
		switch (operation.c) {
			case OperationCategory.LOGICAL:
				let logicalOperation     = <JSONLogicalOperation>operation
				let jsonLogicalOperation = <JSONLogicalOperation>jsonOperation
				switch (operation.o) {
					case SqlOperator.NOT:
						jsonLogicalOperation.v = this.whereClauseToJSON(<JSONBaseOperation>logicalOperation.v, columnAliases, fieldUtils)
						break
					case SqlOperator.AND:
					case SqlOperator.OR:
						jsonLogicalOperation.v = (<JSONBaseOperation[]>logicalOperation.v).map((value) =>
							this.whereClauseToJSON(value, columnAliases, fieldUtils)
						)
						break
					default:
						throw new Error(`Unsupported logical operation '${operation.o}'`)
				}
				break
			case OperationCategory.FUNCTION:
				// TODO: verify that cast of Q object is valid
				let functionOperation: QExistsFunction<any> = <QExistsFunction<any>><any>operation
				let query                                   = functionOperation.getQuery()
				const TreeQueryClass: typeof TreeQuery      = require('../query/facade/TreeQuery').TreeQuery
				let jsonQuery                               = new TreeQueryClass(
					query, columnAliases.entityAliases).toJSON(this, fieldUtils)
				jsonOperation                               = functionOperation.toJSON(jsonQuery)
				break
			case OperationCategory.BOOLEAN:
			case OperationCategory.DATE:
			case OperationCategory.NUMBER:
			case OperationCategory.STRING:
			case OperationCategory.UNTYPED:
				let valueOperation: JSONRawValueOperation<any> = <JSONRawValueOperation<any>>operation
				// All Non logical or exists operations are value operations (eq, isNull, like,
				// etc.)
				let jsonValueOperation: JSONValueOperation = <JSONValueOperation>jsonOperation
				jsonValueOperation.l                       = this.convertLRValue(
					valueOperation.l, columnAliases, fieldUtils)
				let rValue                                 = valueOperation.r
				if (rValue instanceof Array) {
					jsonValueOperation.r = rValue.map((anRValue) => {
						return this.convertLRValue(anRValue, columnAliases, fieldUtils)
					})
				} else {
					jsonValueOperation.r = this.convertLRValue(rValue, columnAliases, fieldUtils)
				}
				break
		}

		return jsonOperation
	}

	private convertLRValue(
		value,
		columnAliases: IFieldColumnAliases<any>,
		fieldUtils: IFieldUtils
	): any {
		value = wrapPrimitive(value)
		switch (typeof value) {
			case 'undefined':
				throw new Error(`'undefined' is not a valid L or R value`)
			default:
				if (value instanceof QOperableField) {
					return value.toJSON(columnAliases, false, this, fieldUtils)
				} // Must be a Field Query
				else {
					let rawFieldQuery: RawFieldQuery<any> = value
					return fieldUtils.getFieldQueryJson(
						rawFieldQuery, columnAliases.entityAliases, this)
				}
		}
	}
}

DI.set(QUERY_UTILS, QueryUtils)

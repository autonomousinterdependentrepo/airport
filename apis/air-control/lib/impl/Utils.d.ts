export declare function strsToNums(strings: string[]): number[];
export declare function objectExists(object: any): boolean;
export declare function valuesEqual(value1: any, value2: any, checkChildObjects?: boolean): boolean;
export declare function compareNumbers(number1: number, number2: number): -1 | 0 | 1;
//# sourceMappingURL=Utils.d.ts.map
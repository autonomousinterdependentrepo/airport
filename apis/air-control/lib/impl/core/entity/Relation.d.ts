import { DbRelation } from '@airport/ground-control';
import { IQEntityInternal } from '../../../lingo/core/entity/Entity';
/**
 * Created by Papa on 4/26/2016.
 */
export declare function QRelation(dbRelation: DbRelation, parentQ: IQEntityInternal<any>): void;
//# sourceMappingURL=Relation.d.ts.map
import { EntityDecorator, MappedSuperclassDecorator, TableDecorator } from '../../../../lingo/core/entity/metadata/EntityDecorators';
/**
 * Created by Papa on 8/20/2016.
 */
export declare const Entity: EntityDecorator;
export declare const Table: TableDecorator;
export declare const MappedSuperclass: MappedSuperclassDecorator;
//# sourceMappingURL=EntityDecorators.d.ts.map
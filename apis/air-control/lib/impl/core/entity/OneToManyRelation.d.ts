import { DbRelation } from '@airport/ground-control';
import { IQEntityInternal } from '../../../lingo/core/entity/Entity';
/**
 * Created by Papa on 10/25/2016.
 */
export declare function QOneToManyRelation(dbRelation: DbRelation, parentQ: IQEntityInternal<any>): void;
//# sourceMappingURL=OneToManyRelation.d.ts.map
import { IQDateField } from "../../../lingo/core/field/DateField";
import { IDateOperation, JSONRawDateOperation } from "../../../lingo/core/operation/DateOperation";
import { ValueOperation } from "./Operation";
/**
 * Created by Papa on 6/20/2016.
 */
export declare class DateOperation extends ValueOperation<Date, JSONRawDateOperation, IQDateField> implements IDateOperation {
    constructor();
}
//# sourceMappingURL=DateOperation.d.ts.map
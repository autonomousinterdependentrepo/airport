export {};
/**
 * Transactional decorator
 *
 * Ex:
 * @Transactional
 * async transactionalMethod() { ... }
 *
 * When decorated with this the decorated method will run
 * in a transactional context.
 */
/*
export interface TransactionalDecorator {
    (): MethodDecorator;
}
*/
//# sourceMappingURL=decorators.js.map
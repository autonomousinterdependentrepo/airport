export var ConstraintMode;
(function (ConstraintMode) {
    ConstraintMode[ConstraintMode["CONSTRAINT"] = 0] = "CONSTRAINT";
    ConstraintMode[ConstraintMode["NO_CONSTRAINT"] = 1] = "NO_CONSTRAINT";
    ConstraintMode[ConstraintMode["PROVIDER_DEFAULT"] = 2] = "PROVIDER_DEFAULT";
})(ConstraintMode || (ConstraintMode = {}));
//# sourceMappingURL=ColumnDecorators.js.map
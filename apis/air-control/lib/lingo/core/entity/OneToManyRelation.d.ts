import { IQEntity } from './Entity';
import { IQRelation } from './Relation';
/**
 * A concrete One-To-Many relation.
 */
export interface IQOneToManyRelation<Entity, IQ extends IQEntity<Entity>> extends IQRelation<Entity, IQ> {
}
//# sourceMappingURL=OneToManyRelation.d.ts.map
# TIQL Generated Entity Query Objects

<!-- TOC -->

- [TIQL Generated Entity Query Objects](#tiql-generated-entity-query-objects)
    - [Entity Query Interface](#entity-query-interface)
    - [Entity Query Implementation](#entity-query-implementation)

<!-- /TOC -->

TIQL builds on the concept of "Q" objects from
[Querydsl](http://www.querydsl.com/).

These Objects are meant to be automatically generated from the actual
entity objects.

TQHS is the reference implementation project that accomplishes this 
auto generation (by running in the background as you code).

## Entity Query Interface

Entity Query interfaces (Ex: [ITask](../examples/tqhs/task.md#entity-query-interface)
& [IGoal](../examples/tqhs/goal.md#entity-query-interface))

## Entity Query Implementation




# Field Query

Field Queries always return a single column list.

## Usage

Here are some [examples](../../examples/query/field.md).

## Reason

Field Queries are mostly useful for sub-queries.
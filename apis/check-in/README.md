# check-in

Airport check-in is the process whereby passengers (data entities)
are accepted by an airline (your application) at the airport prior
to travel (running of the application).
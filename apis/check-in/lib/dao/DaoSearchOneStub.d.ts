import { IObservable } from '@airport/observe';
export declare class DaoSearchOneStub<IEntity> {
    Graph(...args: any[]): IObservable<IEntity>;
    Tree(...args: any[]): IObservable<IEntity>;
}
//# sourceMappingURL=DaoSearchOneStub.d.ts.map
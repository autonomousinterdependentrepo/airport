export declare class DaoFindStub<Entity, EntityArray extends Array<Entity>> {
    Graph(...args: any[]): Promise<EntityArray>;
    Tree(...args: any[]): Promise<EntityArray>;
}
//# sourceMappingURL=DaoFindStub.d.ts.map
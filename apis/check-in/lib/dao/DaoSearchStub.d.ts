import { IObservable } from '@airport/observe';
export declare class DaoSearchStub<Entity, EntityArray extends Array<Entity>> {
    Graph(...args: any[]): IObservable<EntityArray>;
    Tree(...args: any[]): IObservable<EntityArray>;
}
//# sourceMappingURL=DaoSearchStub.d.ts.map
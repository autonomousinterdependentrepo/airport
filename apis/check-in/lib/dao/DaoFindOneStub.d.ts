export declare class DaoFindOneStub<Entity> {
    Graph(...args: any[]): Promise<Entity>;
    Tree(...args: any[]): Promise<Entity>;
}
//# sourceMappingURL=DaoFindOneStub.d.ts.map
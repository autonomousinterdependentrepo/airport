import { IDuo, IEntityCascadeGraph, IEntityCreateProperties, IEntityIdProperties, IEntitySelectProperties, IEntityUpdateColumns, IEntityUpdateProperties, IFieldsSelect, IQEntity, QSchema } from '@airport/air-control';
import { DbEntity, EntityId as DbEntityId } from '@airport/ground-control';
/**
 * Data Manipulation object.
 */
export declare class Duo<Entity, EntitySelect extends IEntitySelectProperties, EntityCreate extends IEntityCreateProperties, EntityUpdateColumns extends IEntityUpdateColumns, EntityUpdate extends IEntityUpdateProperties, EntityId extends IEntityIdProperties, EntityCascadeGraph extends IEntityCascadeGraph, IQE extends IQEntity<Entity>> implements IDuo<Entity, EntitySelect, EntityCreate, EntityUpdateColumns, EntityUpdate, EntityId, EntityCascadeGraph, IQE> {
    select: IFieldsSelect<EntitySelect>;
    private dbEntity;
    constructor(dbEntityId: DbEntityId | DbEntity, qSchema?: QSchema);
    getIdStub(ids: number | string | number[] | string[]): EntityId;
    getIdStubs(ids: number[] | string[] | number[][] | string[][]): EntityId[];
}
export declare function getAllFieldsSelect(dbEntity: DbEntity): IEntitySelectProperties;
export declare const DUO: {
    getAllFieldsSelect: typeof getAllFieldsSelect;
};
//# sourceMappingURL=Duo.d.ts.map
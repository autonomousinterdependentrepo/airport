import { ISelectorManager } from './Selector';
import { ISequenceGenerator } from './SequenceGenerator';
import { IOperationSerializer } from './serialize/OperationSerializer';
import { IQueryResultsDeserializer } from './serialize/QueryResultsDeserializer';
import { IQueryResultsSerializer } from './serialize/QueryResultsSerializer';
export declare const QUERY_RESULTS_DESERIALIZER: import("@airport/di").IDiToken<IQueryResultsDeserializer>;
export declare const QUERY_RESULTS_SERIALIZER: import("@airport/di").IDiToken<IQueryResultsSerializer>;
export declare const OPERATION_SERIALIZER: import("@airport/di").IDiToken<IOperationSerializer>;
export declare const SELECTOR_MANAGER: import("@airport/di").IDiToken<ISelectorManager>;
export declare const SEQUENCE_GENERATOR: import("@airport/di").IDiToken<ISequenceGenerator>;
//# sourceMappingURL=tokens.d.ts.map
export interface RelationColumnReference {
	domain: string;
	schemaName: string;
	entityIndex: number;
	relationIndex: number;
	columnIndex: number;
}
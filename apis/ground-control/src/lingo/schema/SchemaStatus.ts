export enum SchemaStatus {
	CURRENT,
	MISSING,
	NEEDS_UPGRADES
}
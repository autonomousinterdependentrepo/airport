export interface DbSequence {

	// Id Properties
	schemaIndex?: number;
	tableIndex?: number;
	columnIndex?: number;

	// Id Relations

	// Non-Id Properties
	incrementBy?: number;

	// Non-Id Relations

	// Transient Properties

	// Public Methods

}

export enum PlatformType {
	GOOGLE_DOCS,
	IN_MEMORY,
	OFFLINE,
	STUB
}
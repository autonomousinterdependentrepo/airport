export enum ChangeType {
	INSERT_VALUES,
	DELETE_ROWS,
	UPDATE_ROWS
}
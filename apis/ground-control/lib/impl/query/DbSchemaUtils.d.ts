import { ColumnName } from '../../lingo/schema/Property';
export declare function getSchemaName({ domain, name, }: {
    domain: any;
    name: any;
}): string;
export declare function getSchemaNameFromDomainAndName(domainName: any, schemaName: any): string;
export declare function getSequenceName(prefixedTableName: string, columnName: ColumnName): string;
//# sourceMappingURL=DbSchemaUtils.d.ts.map
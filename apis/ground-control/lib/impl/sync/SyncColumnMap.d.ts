import { TableIndex } from "../../lingo/schema/Entity";
import { ColumnMap } from "../query/ColumnMap";
export declare class SyncColumnMap extends ColumnMap {
    constructor(tableIndex: TableIndex, allColumns?: boolean);
}
//# sourceMappingURL=SyncColumnMap.d.ts.map
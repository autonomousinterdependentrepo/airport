import { IStoreDriver } from './lingo/data/StoreDriver';
import { ITransactionalConnector } from './lingo/ITransactionalConnector';
export declare const STORE_DRIVER: import("@airport/di").IDiToken<IStoreDriver>;
export declare const TRANS_CONNECTOR: import("@airport/di").IDiToken<ITransactionalConnector>;
//# sourceMappingURL=tokens.d.ts.map
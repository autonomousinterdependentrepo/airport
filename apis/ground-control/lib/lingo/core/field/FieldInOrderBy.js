/**
 * Order of a sorted field, as specified in the ORDER BY clause.
 */
export var SortOrder;
(function (SortOrder) {
    SortOrder[SortOrder["ASCENDING"] = 0] = "ASCENDING";
    SortOrder[SortOrder["DESCENDING"] = 1] = "DESCENDING";
})(SortOrder || (SortOrder = {}));
//# sourceMappingURL=FieldInOrderBy.js.map
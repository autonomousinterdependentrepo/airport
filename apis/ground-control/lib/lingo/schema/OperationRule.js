"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OperationRuleType;
(function (OperationRuleType) {
    OperationRuleType[OperationRuleType["CREATE"] = 0] = "CREATE";
    OperationRuleType[OperationRuleType["DELETE"] = 1] = "DELETE";
    OperationRuleType[OperationRuleType["SAVE"] = 2] = "SAVE";
    OperationRuleType[OperationRuleType["UPDATE"] = 3] = "UPDATE";
})(OperationRuleType = exports.OperationRuleType || (exports.OperationRuleType = {}));
//# sourceMappingURL=OperationRule.js.map
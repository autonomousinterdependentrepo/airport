export interface DbSequence {
    schemaIndex?: number;
    tableIndex?: number;
    columnIndex?: number;
    incrementBy?: number;
}
//# sourceMappingURL=Sequence.d.ts.map
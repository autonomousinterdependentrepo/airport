export declare enum SchemaStatus {
    CURRENT = 0,
    MISSING = 1,
    NEEDS_UPGRADES = 2
}
//# sourceMappingURL=SchemaStatus.d.ts.map
export var JsonStatementType;
(function (JsonStatementType) {
    JsonStatementType[JsonStatementType["ENTITY_QUERY"] = 0] = "ENTITY_QUERY";
    JsonStatementType[JsonStatementType["NON_ENTITY_QUERY"] = 1] = "NON_ENTITY_QUERY";
})(JsonStatementType || (JsonStatementType = {}));
//# sourceMappingURL=Query.js.map
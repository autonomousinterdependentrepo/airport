export var TransactionType;
(function (TransactionType) {
    TransactionType[TransactionType["LOCAL"] = 0] = "LOCAL";
    TransactionType[TransactionType["REMOTE_SYNC"] = 1] = "REMOTE_SYNC";
})(TransactionType || (TransactionType = {}));
//# sourceMappingURL=TransactionType.js.map
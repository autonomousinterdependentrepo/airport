export declare enum ChangeType {
    INSERT_VALUES = 0,
    DELETE_ROWS = 1,
    UPDATE_ROWS = 2
}
//# sourceMappingURL=ChangeType.d.ts.map
export var PlatformType;
(function (PlatformType) {
    PlatformType[PlatformType["GOOGLE_DOCS"] = 0] = "GOOGLE_DOCS";
    PlatformType[PlatformType["IN_MEMORY"] = 1] = "IN_MEMORY";
    PlatformType[PlatformType["OFFLINE"] = 2] = "OFFLINE";
    PlatformType[PlatformType["STUB"] = 3] = "STUB";
})(PlatformType || (PlatformType = {}));
//# sourceMappingURL=PatformType.js.map
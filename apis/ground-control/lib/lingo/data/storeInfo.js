export var StoreType;
(function (StoreType) {
    StoreType[StoreType["COCKROACHDB"] = 0] = "COCKROACHDB";
    StoreType[StoreType["MYSQL"] = 1] = "MYSQL";
    StoreType[StoreType["POSTGRESQL"] = 2] = "POSTGRESQL";
    StoreType[StoreType["REMOTE"] = 3] = "REMOTE";
    StoreType[StoreType["SQLITE_CORDOVA"] = 4] = "SQLITE_CORDOVA";
    StoreType[StoreType["SQLJS"] = 5] = "SQLJS";
})(StoreType || (StoreType = {}));
export var IdGeneration;
(function (IdGeneration) {
    IdGeneration[IdGeneration["ENTITY_CHANGE_ID"] = 0] = "ENTITY_CHANGE_ID";
})(IdGeneration || (IdGeneration = {}));
//# sourceMappingURL=storeInfo.js.map
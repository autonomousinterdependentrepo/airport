/**
 * Created by Papa on 4/16/2017.
 */

export enum RepositoryEntityType {
	NOT_REPOSITORY_ENTITY,
	REPOSITORY_ENTITY
}

export const REPOSITORY_ID = 'REPOSITORY_ID';

export const REPOSITORY_FIELD = 'repository';
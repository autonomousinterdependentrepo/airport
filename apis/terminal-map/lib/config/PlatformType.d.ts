export declare enum PlatformType {
    GOOGLE_DOCS = 0,
    IN_MEMORY = 1,
    OFFLINE = 2,
    STUB = 3
}
//# sourceMappingURL=PlatformType.d.ts.map
/**
 * Created by Papa on 4/16/2017.
 */
export declare enum RepositoryEntityType {
    NOT_REPOSITORY_ENTITY = 0,
    REPOSITORY_ENTITY = 1
}
export declare const REPOSITORY_ID = "REPOSITORY_ID";
export declare const REPOSITORY_FIELD = "repository";
//# sourceMappingURL=RepositoryEntityType.d.ts.map
/**
 * Created by Papa on 4/16/2017.
 */
export var RepositoryEntityType;
(function (RepositoryEntityType) {
    RepositoryEntityType[RepositoryEntityType["NOT_REPOSITORY_ENTITY"] = 0] = "NOT_REPOSITORY_ENTITY";
    RepositoryEntityType[RepositoryEntityType["REPOSITORY_ENTITY"] = 1] = "REPOSITORY_ENTITY";
})(RepositoryEntityType || (RepositoryEntityType = {}));
export const REPOSITORY_ID = 'REPOSITORY_ID';
export const REPOSITORY_FIELD = 'repository';
//# sourceMappingURL=RepositoryEntityType.js.map
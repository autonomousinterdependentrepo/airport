import { ITransactionManager } from './orchestration/TransactionManager';
import { ITerminalStore } from './store/TerminalStore';
export declare const TERMINAL_STORE: import("@airport/di").IDiToken<ITerminalStore>;
export declare const TRANSACTION_MANAGER: import("@airport/di").IDiToken<ITransactionManager>;
//# sourceMappingURL=tokens.d.ts.map
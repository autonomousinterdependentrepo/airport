export var QueryType;
(function (QueryType) {
    QueryType[QueryType["PREPARED"] = 0] = "PREPARED";
    QueryType[QueryType["DYNAMIC"] = 1] = "DYNAMIC";
})(QueryType || (QueryType = {}));
//# sourceMappingURL=Query.js.map
import { IQueryValidator } from './query/QueryValidator';
import { IQueryWebService } from './query/QueryWs';
export declare const QUERY_WEB_SERVICE: import("@airport/di").IDiToken<IQueryWebService>;
export declare const QUERY_VALIDATOR: import("@airport/di").IDiToken<IQueryValidator>;
//# sourceMappingURL=tokens.d.ts.map
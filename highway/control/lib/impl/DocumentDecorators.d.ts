import { PropertyDecorator } from '@airport/air-control';
import { DocumentConfiguration } from '../lingo/DocumentDecorators';
export declare function Document<T>(documentConfiguration?: DocumentConfiguration<T>): PropertyDecorator;
//# sourceMappingURL=DocumentDecorators.d.ts.map
export interface ISchemaCollector {
    collect(): Promise<string[]>;
}
export declare class SchemaCollector implements ISchemaCollector {
    collect(): Promise<string[]>;
}
//# sourceMappingURL=SchemaCollector.d.ts.map
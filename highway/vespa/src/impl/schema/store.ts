import {
	IVespaSchemaStore,
	VespaSchemaStore
} from './VespaSchemaStore';

export const store: IVespaSchemaStore = new VespaSchemaStore();

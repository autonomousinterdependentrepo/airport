


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ILoggedErrorStackTrace {
	
	// Id Properties
	id: number;

	// Id Relations

	// Non-Id Properties
	stackHash?: string;
	stack?: string;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



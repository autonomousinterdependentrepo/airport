export interface ILoggedErrorStackTrace {
    id: number;
    stackHash?: string;
    stack?: string;
}
//# sourceMappingURL=loggederrorstacktrace.d.ts.map
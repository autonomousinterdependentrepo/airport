export * from './logentry';
export * from './logentrytype';
export * from './logentryvalue';
export * from './loggederror';
export * from './loggederrorstacktrace';
//# sourceMappingURL=interfaces.d.ts.map
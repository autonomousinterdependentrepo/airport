import { ILogEntryDao } from './dao/LogEntryDao';
import { ILogEntryTypeDao } from './dao/LogEntryTypeDao';
import { ILogEntryValueDao } from './dao/LogEntryValueDao';
export declare const LOG_ENTRY_DAO: import("@airport/di").DiToken<ILogEntryDao>;
export declare const LOG_ENTRY_TYPE_DAO: import("@airport/di").DiToken<ILogEntryTypeDao>;
export declare const LOG_ENTRY_VALUE_DAO: import("@airport/di").DiToken<ILogEntryValueDao>;

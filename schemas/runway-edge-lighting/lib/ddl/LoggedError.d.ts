import { LogEntry } from "./LogEntry";
import { LoggedErrorStackTrace } from "./LoggedErrorStackTrace";
export declare class LoggedError {
    logEntry: LogEntry;
    stackTrace: LoggedErrorStackTrace;
}
//# sourceMappingURL=LoggedError.d.ts.map
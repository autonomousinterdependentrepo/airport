export declare enum LogLevel {
    FATAL = 0,
    ERROR = 1,
    WARNING = 2,
    INFO = 3,
    DEBUG = 4,
    TRACE = 5
}
export declare type SetLogLevel = LogLevel.INFO | LogLevel.DEBUG | LogLevel.TRACE;
//# sourceMappingURL=LogLevel.d.ts.map
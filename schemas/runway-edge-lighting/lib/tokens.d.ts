import { ILogEntryDao } from './dao/LogEntryDao';
import { ILogEntryTypeDao } from './dao/LogEntryTypeDao';
import { ILogEntryValueDao } from './dao/LogEntryValueDao';
export declare const LOG_ENTRY_DAO: import("@airport/di").IDiToken<ILogEntryDao>;
export declare const LOG_ENTRY_TYPE_DAO: import("@airport/di").IDiToken<ILogEntryTypeDao>;
export declare const LOG_ENTRY_VALUE_DAO: import("@airport/di").IDiToken<ILogEntryValueDao>;
//# sourceMappingURL=tokens.d.ts.map
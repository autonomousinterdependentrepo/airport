"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("@airport/di");
exports.NPMJS_ORG___AIRPORT_TRAFFIC_PATTERN_QSCHEMA = 'NPMJS_ORG___AIRPORT_TRAFFIC_PATTERN_QSCHEMA';
exports.NPMJS_ORG___AIRPORT_TRAFFIC_PATTERN_DAOS = di_1.diToken();
exports.NPMJS_ORG___AIRPORT_TRAFFIC_PATTERN_DUOS = di_1.diToken();
exports.SCHEMA_COLUMN_DAO = di_1.diToken();
exports.SCHEMA_DAO = di_1.diToken();
exports.SCHEMA_ENTITY_DAO = di_1.diToken();
exports.SCHEMA_PROPERTY_COLUMN_DAO = di_1.diToken();
exports.SCHEMA_PROPERTY_DAO = di_1.diToken();
exports.SCHEMA_REFERENCE_DAO = di_1.diToken();
exports.SCHEMA_RELATION_COLUMN_DAO = di_1.diToken();
exports.SCHEMA_RELATION_DAO = di_1.diToken();
exports.SCHEMA_VERSION_DAO = di_1.diToken();
exports.SCHEMA_VERSION_DUO = di_1.diToken();
//# sourceMappingURL=diTokens.js.map
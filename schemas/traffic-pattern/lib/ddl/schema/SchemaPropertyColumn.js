var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Entity, Id, JoinColumn, ManyToOne, Table } from '@airport/air-control';
import { VersionedSchemaObject } from './VersionedSchemaObject';
/**
 * Many-to-Many between Columns and properties
 */
let SchemaPropertyColumn = class SchemaPropertyColumn extends VersionedSchemaObject {
};
__decorate([
    Id(),
    ManyToOne(),
    JoinColumn({ name: 'SCHEMA_COLUMN_ID', referencedColumnName: 'ID', nullable: false })
], SchemaPropertyColumn.prototype, "column", void 0);
__decorate([
    Id(),
    ManyToOne(),
    JoinColumn({ name: 'SCHEMA_PROPERTY_ID', referencedColumnName: 'ID', nullable: false })
], SchemaPropertyColumn.prototype, "property", void 0);
SchemaPropertyColumn = __decorate([
    Entity()
    // TODO: rename table name to SCHEMA_PROPERTY_COLUMNS
    ,
    Table({
        name: 'SCHEMA_PROPERTY_COLUMNS'
    })
], SchemaPropertyColumn);
export { SchemaPropertyColumn };
//# sourceMappingURL=SchemaPropertyColumn.js.map
export declare const MAPPED_SUPERCLASS: {
    type: string;
    path: string;
    parentClassName: any;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                JoinColumn: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        JoinColumn: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        ManyToOne: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                ManyToOne: any;
                MappedSuperclass: any;
                SchemaVersion: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        SchemaVersion: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "./SchemaVersion": any;
            };
        };
        properties: ({
            decorators: {
                name: string;
                values: {
                    name: string;
                    referencedColumnName: string;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            entity: {
                type: string;
                path: string;
                parentClassName: any;
                isSuperclass: boolean;
                ids: {
                    allocationSize: number;
                    decorators: {
                        name: string;
                        values: {
                            allocationSize: number;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    ownerEntity: any;
                    nonArrayType: string;
                    primitive: string;
                    index: number;
                }[];
                docEntry: {
                    decorators: {
                        name: string;
                        values: {
                            name: string;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    fileImports: {
                        importMapByObjectAsName: {
                            Column: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    Column: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    DbNumber: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    DbString: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    Entity: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    Id: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    JoinColumn: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    ManyToOne: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    OneToMany: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    SequenceGenerator: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    Table: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    Transient: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            DbNumber: any;
                            DbString: any;
                            Entity: any;
                            Id: any;
                            JoinColumn: any;
                            ManyToOne: any;
                            OneToMany: any;
                            SequenceGenerator: any;
                            Table: any;
                            Transient: any;
                            SchemaVersionId: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    SchemaVersionId: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    SchemaVersionInteger: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    SchemaVersionMajor: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    SchemaVersionMinor: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    SchemaVersionPatch: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    SchemaVersionString: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            SchemaVersionInteger: any;
                            SchemaVersionMajor: any;
                            SchemaVersionMinor: any;
                            SchemaVersionPatch: any;
                            SchemaVersionString: any;
                            Schema: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    Schema: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            SchemaEntity: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    SchemaEntity: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            SchemaReference: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    SchemaReference: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            ISchemaEntity: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    ISchemaEntity: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            ISchemaReference: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    ISchemaReference: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                        };
                        importMapByModulePath: {
                            "@airport/air-control": any;
                            "@airport/ground-control": any;
                            "./Schema": any;
                            "./SchemaEntity": any;
                            "./SchemaReference": any;
                            "../../generated/schema/schemaentity": any;
                            "../../generated/schema/schemareference": any;
                        };
                    };
                    properties: ({
                        allocationSize: number;
                        decorators: {
                            name: string;
                            values: {
                                allocationSize: number;
                            }[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        nonArrayType: string;
                        primitive: string;
                        index: number;
                        entity?: undefined;
                        isArray?: undefined;
                        isMap?: undefined;
                        mapValueType?: undefined;
                        mapValueIsPrimitive?: undefined;
                        mapKeyName?: undefined;
                        mapKeyType?: undefined;
                    } | {
                        decorators: {
                            name: string;
                            values: {
                                name: string;
                                nullable: boolean;
                            }[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        nonArrayType: string;
                        primitive: string;
                        index: number;
                        allocationSize?: undefined;
                        entity?: undefined;
                        isArray?: undefined;
                        isMap?: undefined;
                        mapValueType?: undefined;
                        mapValueIsPrimitive?: undefined;
                        mapKeyName?: undefined;
                        mapKeyType?: undefined;
                    } | {
                        decorators: {
                            name: string;
                            values: {
                                name: string;
                                nullable: boolean;
                            }[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        nonArrayType: string;
                        entity: {
                            type: string;
                            path: string;
                            parentClassName: any;
                            isSuperclass: boolean;
                            ids: {
                                decorators: {
                                    name: string;
                                    values: {
                                        name: string;
                                        nullable: boolean;
                                    }[];
                                }[];
                                isGenerated: boolean;
                                isId: boolean;
                                isMappedSuperclass: boolean;
                                isTransient: boolean;
                                name: string;
                                type: string;
                                ownerEntity: any;
                                nonArrayType: string;
                                primitive: string;
                                index: number;
                            }[];
                            docEntry: {
                                decorators: {
                                    name: string;
                                    values: {
                                        name: string;
                                    }[];
                                }[];
                                isGenerated: boolean;
                                isId: boolean;
                                isMappedSuperclass: boolean;
                                isTransient: boolean;
                                name: string;
                                type: string;
                                fileImports: {
                                    importMapByObjectAsName: {
                                        Column: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                Column: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbNumber: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbString: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Entity: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Id: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                JoinColumn: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                ManyToOne: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                OneToMany: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Table: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbBoolean?: undefined;
                                                Json?: undefined;
                                                TableConfiguration?: undefined;
                                                Transient?: undefined;
                                            };
                                            path: string;
                                        };
                                        DbNumber: any;
                                        DbString: any;
                                        Entity: any;
                                        Id: any;
                                        JoinColumn: any;
                                        ManyToOne: any;
                                        OneToMany: any;
                                        Table: any;
                                        PackageName: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                PackageName: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                SchemaIndex: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                SchemaName: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                SchemaScope: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                SchemaStatus: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaIndex: any;
                                        SchemaName: any;
                                        SchemaScope: any;
                                        SchemaStatus: any;
                                        Domain: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                Domain: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaVersion: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaVersion: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        DbBoolean?: undefined;
                                        Json?: undefined;
                                        TableConfiguration?: undefined;
                                        Transient?: undefined;
                                        EntityId?: undefined;
                                        EntityIsLocal?: undefined;
                                        EntityIsRepositoryEntity?: undefined;
                                        EntityName?: undefined;
                                        TableIndex?: undefined;
                                        SchemaColumn?: undefined;
                                        SchemaOperation?: undefined;
                                        SchemaProperty?: undefined;
                                        SchemaRelation?: undefined;
                                        VersionedSchemaObject?: undefined;
                                        ISchemaColumn?: undefined;
                                        ISchemaProperty?: undefined;
                                        SchemaReferenceIndex?: undefined;
                                    };
                                    importMapByModulePath: {
                                        "@airport/air-control": any;
                                        "@airport/ground-control": any;
                                        "@airport/territory": any;
                                        "./SchemaVersion": any;
                                        "./SchemaColumn"?: undefined;
                                        "./SchemaOperation"?: undefined;
                                        "./SchemaProperty"?: undefined;
                                        "./SchemaRelation"?: undefined;
                                        "./VersionedSchemaObject"?: undefined;
                                        "../../generated/schema/schemacolumn"?: undefined;
                                        "../../generated/schema/schemaproperty"?: undefined;
                                    };
                                };
                                properties: ({
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    primitive: string;
                                    index: number;
                                    fromProject?: undefined;
                                    otherSchemaDbEntity?: undefined;
                                    isArray?: undefined;
                                    entity?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            referencedColumnName: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    fromProject: string;
                                    otherSchemaDbEntity: {
                                        columnMap: any;
                                        columns: ({
                                            entity: any;
                                            id: any;
                                            index: number;
                                            isGenerated: boolean;
                                            manyRelationColumns: any[];
                                            name: string;
                                            notNull: boolean;
                                            oneRelationColumns: any[];
                                            propertyColumnMap: any;
                                            propertyColumns: {
                                                column: any;
                                                property: any;
                                                sinceVersion: any;
                                            }[];
                                            sinceVersion: any;
                                            type: number;
                                            idIndex: number;
                                        } | {
                                            entity: any;
                                            id: any;
                                            index: number;
                                            isGenerated: boolean;
                                            manyRelationColumns: any[];
                                            name: string;
                                            notNull: boolean;
                                            oneRelationColumns: any[];
                                            propertyColumnMap: any;
                                            propertyColumns: {
                                                column: any;
                                                property: any;
                                                sinceVersion: any;
                                            }[];
                                            sinceVersion: any;
                                            type: number;
                                            idIndex?: undefined;
                                        })[];
                                        idColumns: {
                                            entity: any;
                                            id: any;
                                            index: number;
                                            isGenerated: boolean;
                                            manyRelationColumns: any[];
                                            name: string;
                                            notNull: boolean;
                                            oneRelationColumns: any[];
                                            propertyColumnMap: any;
                                            propertyColumns: {
                                                column: any;
                                                property: any;
                                                sinceVersion: any;
                                            }[];
                                            sinceVersion: any;
                                            type: number;
                                            idIndex: number;
                                        }[];
                                        idColumnMap: any;
                                        id: any;
                                        index: number;
                                        isLocal: boolean;
                                        isRepositoryEntity: boolean;
                                        name: string;
                                        propertyMap: any;
                                        properties: ({
                                            propertyColumns: {
                                                column: any;
                                                property: any;
                                                sinceVersion: any;
                                            }[];
                                            entity: any;
                                            id: any;
                                            index: number;
                                            isId: boolean;
                                            name: string;
                                            relation: any;
                                            sinceVersion: any;
                                        } | {
                                            propertyColumns: any[];
                                            entity: any;
                                            id: any;
                                            index: number;
                                            isId: boolean;
                                            name: string;
                                            relation: {
                                                isId: boolean;
                                                oneToManyElems: any;
                                                relationType: number;
                                                id: any;
                                                index: number;
                                                property: any;
                                                manyRelationColumns: any[];
                                                oneRelationColumns: any[];
                                                relationEntity: any;
                                                sinceVersion: any;
                                            }[];
                                            sinceVersion: any;
                                        })[];
                                        relationReferences: any[];
                                        relations: {
                                            isId: boolean;
                                            oneToManyElems: any;
                                            relationType: number;
                                            id: any;
                                            index: number;
                                            property: any;
                                            manyRelationColumns: any[];
                                            oneRelationColumns: any[];
                                            relationEntity: any;
                                            sinceVersion: any;
                                        }[];
                                        schemaVersion: any;
                                        sinceVersion: any;
                                        tableConfig: any;
                                    };
                                    index: number;
                                    primitive?: undefined;
                                    isArray?: undefined;
                                    entity?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            mappedBy: string;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    isArray: boolean;
                                    nonArrayType: string;
                                    entity: any;
                                    index: number;
                                    primitive?: undefined;
                                    fromProject?: undefined;
                                    otherSchemaDbEntity?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            referencedColumnName: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    entity: any;
                                    index: number;
                                    primitive?: undefined;
                                    fromProject?: undefined;
                                    otherSchemaDbEntity?: undefined;
                                    isArray?: undefined;
                                })[];
                                methodSignatures: any[];
                                constructors: {
                                    parameters: any[];
                                    returnType: string;
                                }[];
                            };
                            implementedInterfaceNames: any[];
                            location?: undefined;
                            parentEntity?: undefined;
                        };
                        index: number;
                        allocationSize?: undefined;
                        primitive?: undefined;
                        isArray?: undefined;
                        isMap?: undefined;
                        mapValueType?: undefined;
                        mapValueIsPrimitive?: undefined;
                        mapKeyName?: undefined;
                        mapKeyType?: undefined;
                    } | {
                        decorators: {
                            name: string;
                            values: {
                                mappedBy: string;
                            }[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        isArray: boolean;
                        nonArrayType: string;
                        entity: {
                            type: string;
                            path: string;
                            parentClassName: string;
                            location: string;
                            isSuperclass: boolean;
                            ids: {
                                decorators: {
                                    name: string;
                                    values: any[];
                                }[];
                                isGenerated: boolean;
                                isId: boolean;
                                isMappedSuperclass: boolean;
                                isTransient: boolean;
                                name: string;
                                type: string;
                                ownerEntity: any;
                                nonArrayType: string;
                                primitive: string;
                                index: number;
                            }[];
                            docEntry: {
                                decorators: {
                                    name: string;
                                    values: {
                                        name: string;
                                    }[];
                                }[];
                                isGenerated: boolean;
                                isId: boolean;
                                isMappedSuperclass: boolean;
                                isTransient: boolean;
                                name: string;
                                type: string;
                                fileImports: {
                                    importMapByObjectAsName: {
                                        Column: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                Column: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbBoolean: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbNumber: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbString: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Entity: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Id: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                JoinColumn: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Json: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                ManyToOne: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                OneToMany: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Table: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                TableConfiguration: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Transient: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        DbBoolean: any;
                                        DbNumber: any;
                                        DbString: any;
                                        Entity: any;
                                        Id: any;
                                        JoinColumn: any;
                                        Json: any;
                                        ManyToOne: any;
                                        OneToMany: any;
                                        Table: any;
                                        TableConfiguration: any;
                                        Transient: any;
                                        EntityId: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                EntityId: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                EntityIsLocal: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                EntityIsRepositoryEntity: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                EntityName: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                TableIndex: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        EntityIsLocal: any;
                                        EntityIsRepositoryEntity: any;
                                        EntityName: any;
                                        TableIndex: any;
                                        SchemaColumn: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaColumn: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaOperation: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaOperation: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaProperty: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaProperty: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaRelation: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaRelation: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaVersion: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaVersion: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        VersionedSchemaObject: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                VersionedSchemaObject: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        ISchemaColumn: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                ISchemaColumn: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        ISchemaProperty: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                ISchemaProperty: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        PackageName?: undefined;
                                        SchemaIndex?: undefined;
                                        SchemaName?: undefined;
                                        SchemaScope?: undefined;
                                        SchemaStatus?: undefined;
                                        Domain?: undefined;
                                        SchemaReferenceIndex?: undefined;
                                    };
                                    importMapByModulePath: {
                                        "@airport/air-control": any;
                                        "@airport/ground-control": any;
                                        "./SchemaColumn": any;
                                        "./SchemaOperation": any;
                                        "./SchemaProperty": any;
                                        "./SchemaRelation": any;
                                        "./SchemaVersion": any;
                                        "./VersionedSchemaObject": any;
                                        "../../generated/schema/schemacolumn": any;
                                        "../../generated/schema/schemaproperty": any;
                                        "@airport/territory"?: undefined;
                                    };
                                };
                                properties: ({
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    primitive: string;
                                    index: number;
                                    entity?: undefined;
                                    isArray?: undefined;
                                    isMap?: undefined;
                                    mapValueType?: undefined;
                                    mapValueIsPrimitive?: undefined;
                                    mapKeyName?: undefined;
                                    mapKeyType?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            referencedColumnName: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    entity: any;
                                    index: number;
                                    primitive?: undefined;
                                    isArray?: undefined;
                                    isMap?: undefined;
                                    mapValueType?: undefined;
                                    mapValueIsPrimitive?: undefined;
                                    mapKeyName?: undefined;
                                    mapKeyType?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            mappedBy: string;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    isArray: boolean;
                                    nonArrayType: string;
                                    entity: {
                                        type: string;
                                        path: string;
                                        parentClassName: string;
                                        location: string;
                                        isSuperclass: boolean;
                                        ids: {
                                            decorators: {
                                                name: string;
                                                values: any[];
                                            }[];
                                            isGenerated: boolean;
                                            isId: boolean;
                                            isMappedSuperclass: boolean;
                                            isTransient: boolean;
                                            name: string;
                                            type: string;
                                            ownerEntity: any;
                                            nonArrayType: string;
                                            primitive: string;
                                            index: number;
                                        }[];
                                        docEntry: {
                                            decorators: {
                                                name: string;
                                                values: {
                                                    name: string;
                                                }[];
                                            }[];
                                            isGenerated: boolean;
                                            isId: boolean;
                                            isMappedSuperclass: boolean;
                                            isTransient: boolean;
                                            name: string;
                                            type: string;
                                            fileImports: {
                                                importMapByObjectAsName: {
                                                    Column: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            Column: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            DbBoolean: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            DbNumber: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            DbString: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Entity: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Id: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            JoinColumn: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ManyToOne: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            OneToMany: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Table: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            GeneratedValue?: undefined;
                                                            Json?: undefined;
                                                        };
                                                        path: string;
                                                    };
                                                    DbBoolean: any;
                                                    DbNumber: any;
                                                    DbString: any;
                                                    Entity: any;
                                                    Id: any;
                                                    JoinColumn: any;
                                                    ManyToOne: any;
                                                    OneToMany: any;
                                                    Table: any;
                                                    ColumnId: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            ColumnId: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ColumnIndex: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ColumnName: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ColumnNotNull: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ColumnPrecision: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ColumnScale: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            IdColumnOnlyIndex: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            SchemaColumnAllocationSize: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            SchemaColumnIsGenerated: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            SQLDataType: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    ColumnIndex: any;
                                                    ColumnName: any;
                                                    ColumnNotNull: any;
                                                    ColumnPrecision: any;
                                                    ColumnScale: any;
                                                    IdColumnOnlyIndex: any;
                                                    SchemaColumnAllocationSize: any;
                                                    SchemaColumnIsGenerated: any;
                                                    SQLDataType: any;
                                                    SchemaEntity: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            SchemaEntity: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    SchemaPropertyColumn: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            SchemaPropertyColumn: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    SchemaRelationColumn: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            SchemaRelationColumn: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    VersionedSchemaObject: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            VersionedSchemaObject: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    GeneratedValue?: undefined;
                                                    Json?: undefined;
                                                    Operation_Id?: undefined;
                                                    Operation_Name?: undefined;
                                                    Operation_Rule?: undefined;
                                                    Operation_Type?: undefined;
                                                };
                                                importMapByModulePath: {
                                                    "@airport/air-control": any;
                                                    "@airport/ground-control": any;
                                                    "./SchemaEntity": any;
                                                    "./SchemaPropertyColumn": any;
                                                    "./SchemaRelationColumn": any;
                                                    "./VersionedSchemaObject": any;
                                                };
                                            };
                                            properties: ({
                                                decorators: {
                                                    name: string;
                                                    values: {
                                                        name: string;
                                                        nullable: boolean;
                                                    }[];
                                                }[];
                                                isGenerated: boolean;
                                                isId: boolean;
                                                isMappedSuperclass: boolean;
                                                isTransient: boolean;
                                                name: string;
                                                type: string;
                                                ownerEntity: any;
                                                nonArrayType: string;
                                                primitive: string;
                                                index: number;
                                                entity?: undefined;
                                                isArray?: undefined;
                                            } | {
                                                decorators: {
                                                    name: string;
                                                    values: {
                                                        name: string;
                                                    }[];
                                                }[];
                                                isGenerated: boolean;
                                                isId: boolean;
                                                isMappedSuperclass: boolean;
                                                isTransient: boolean;
                                                name: string;
                                                type: string;
                                                ownerEntity: any;
                                                nonArrayType: string;
                                                primitive: string;
                                                index: number;
                                                entity?: undefined;
                                                isArray?: undefined;
                                            } | {
                                                decorators: {
                                                    name: string;
                                                    values: {
                                                        name: string;
                                                        referencedColumnName: string;
                                                        nullable: boolean;
                                                    }[];
                                                }[];
                                                isGenerated: boolean;
                                                isId: boolean;
                                                isMappedSuperclass: boolean;
                                                isTransient: boolean;
                                                name: string;
                                                type: string;
                                                ownerEntity: any;
                                                nonArrayType: string;
                                                entity: any;
                                                index: number;
                                                primitive?: undefined;
                                                isArray?: undefined;
                                            } | {
                                                decorators: {
                                                    name: string;
                                                    values: {
                                                        mappedBy: string;
                                                    }[];
                                                }[];
                                                isGenerated: boolean;
                                                isId: boolean;
                                                isMappedSuperclass: boolean;
                                                isTransient: boolean;
                                                name: string;
                                                type: string;
                                                ownerEntity: any;
                                                isArray: boolean;
                                                nonArrayType: string;
                                                entity: {
                                                    type: string;
                                                    path: string;
                                                    parentClassName: string;
                                                    location: string;
                                                    isSuperclass: boolean;
                                                    ids: {
                                                        decorators: {
                                                            name: string;
                                                            values: {
                                                                name: string;
                                                                referencedColumnName: string;
                                                                nullable: boolean;
                                                            }[];
                                                        }[];
                                                        isGenerated: boolean;
                                                        isId: boolean;
                                                        isMappedSuperclass: boolean;
                                                        isTransient: boolean;
                                                        name: string;
                                                        type: string;
                                                        ownerEntity: any;
                                                        nonArrayType: string;
                                                        entity: any;
                                                        index: number;
                                                    }[];
                                                    docEntry: {
                                                        decorators: {
                                                            name: string;
                                                            values: {
                                                                name: string;
                                                            }[];
                                                        }[];
                                                        isGenerated: boolean;
                                                        isId: boolean;
                                                        isMappedSuperclass: boolean;
                                                        isTransient: boolean;
                                                        name: string;
                                                        type: string;
                                                        fileImports: {
                                                            importMapByObjectAsName: {
                                                                Entity: {
                                                                    fileImports: any;
                                                                    isLocal: boolean;
                                                                    objectMapByAsName: {
                                                                        Entity: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                        Id: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                        JoinColumn: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                        ManyToOne: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                        Table: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                    };
                                                                    path: string;
                                                                };
                                                                Id: any;
                                                                JoinColumn: any;
                                                                ManyToOne: any;
                                                                Table: any;
                                                                SchemaColumn: {
                                                                    fileImports: any;
                                                                    isLocal: boolean;
                                                                    objectMapByAsName: {
                                                                        SchemaColumn: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                    };
                                                                    path: string;
                                                                };
                                                                SchemaProperty: {
                                                                    fileImports: any;
                                                                    isLocal: boolean;
                                                                    objectMapByAsName: {
                                                                        SchemaProperty: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                    };
                                                                    path: string;
                                                                };
                                                                VersionedSchemaObject: {
                                                                    fileImports: any;
                                                                    isLocal: boolean;
                                                                    objectMapByAsName: {
                                                                        VersionedSchemaObject: {
                                                                            asName: string;
                                                                            moduleImport: any;
                                                                            sourceName: string;
                                                                        };
                                                                    };
                                                                    path: string;
                                                                };
                                                            };
                                                            importMapByModulePath: {
                                                                "@airport/air-control": any;
                                                                "./SchemaColumn": any;
                                                                "./SchemaProperty": any;
                                                                "./VersionedSchemaObject": any;
                                                            };
                                                        };
                                                        properties: {
                                                            decorators: {
                                                                name: string;
                                                                values: {
                                                                    name: string;
                                                                    referencedColumnName: string;
                                                                    nullable: boolean;
                                                                }[];
                                                            }[];
                                                            isGenerated: boolean;
                                                            isId: boolean;
                                                            isMappedSuperclass: boolean;
                                                            isTransient: boolean;
                                                            name: string;
                                                            type: string;
                                                            ownerEntity: any;
                                                            nonArrayType: string;
                                                            entity: any;
                                                            index: number;
                                                        }[];
                                                        methodSignatures: any[];
                                                        constructors: {
                                                            parameters: any[];
                                                            returnType: string;
                                                        }[];
                                                    };
                                                    implementedInterfaceNames: any[];
                                                    parentEntity: any;
                                                };
                                                index: number;
                                                primitive?: undefined;
                                            })[];
                                            methodSignatures: any[];
                                            constructors: {
                                                parameters: any[];
                                                returnType: string;
                                            }[];
                                        };
                                        implementedInterfaceNames: any[];
                                        parentEntity: any;
                                    };
                                    index: number;
                                    primitive?: undefined;
                                    isMap?: undefined;
                                    mapValueType?: undefined;
                                    mapValueIsPrimitive?: undefined;
                                    mapKeyName?: undefined;
                                    mapKeyType?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            mappedBy: string;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    isArray: boolean;
                                    nonArrayType: string;
                                    entity: {
                                        type: string;
                                        path: string;
                                        parentClassName: string;
                                        location: string;
                                        isSuperclass: boolean;
                                        ids: {
                                            decorators: {
                                                name: string;
                                                values: any[];
                                            }[];
                                            isGenerated: boolean;
                                            isId: boolean;
                                            isMappedSuperclass: boolean;
                                            isTransient: boolean;
                                            name: string;
                                            type: string;
                                            ownerEntity: any;
                                            nonArrayType: string;
                                            primitive: string;
                                            index: number;
                                        }[];
                                        docEntry: {
                                            decorators: {
                                                name: string;
                                                values: {
                                                    name: string;
                                                }[];
                                            }[];
                                            isGenerated: boolean;
                                            isId: boolean;
                                            isMappedSuperclass: boolean;
                                            isTransient: boolean;
                                            name: string;
                                            type: string;
                                            fileImports: {
                                                importMapByObjectAsName: {
                                                    Column: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            Column: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            DbNumber: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            DbString: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Entity: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            GeneratedValue: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Id: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            JoinColumn: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Json: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            ManyToOne: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Table: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            DbBoolean?: undefined;
                                                            OneToMany?: undefined;
                                                        };
                                                        path: string;
                                                    };
                                                    DbNumber: any;
                                                    DbString: any;
                                                    Entity: any;
                                                    GeneratedValue: any;
                                                    Id: any;
                                                    JoinColumn: any;
                                                    Json: any;
                                                    ManyToOne: any;
                                                    Table: any;
                                                    Operation_Id: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            Operation_Id: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Operation_Name: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Operation_Rule: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                            Operation_Type: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    Operation_Name: any;
                                                    Operation_Rule: any;
                                                    Operation_Type: any;
                                                    SchemaEntity: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            SchemaEntity: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    VersionedSchemaObject: {
                                                        fileImports: any;
                                                        isLocal: boolean;
                                                        objectMapByAsName: {
                                                            VersionedSchemaObject: {
                                                                asName: string;
                                                                moduleImport: any;
                                                                sourceName: string;
                                                            };
                                                        };
                                                        path: string;
                                                    };
                                                    DbBoolean?: undefined;
                                                    OneToMany?: undefined;
                                                    ColumnId?: undefined;
                                                    ColumnIndex?: undefined;
                                                    ColumnName?: undefined;
                                                    ColumnNotNull?: undefined;
                                                    ColumnPrecision?: undefined;
                                                    ColumnScale?: undefined;
                                                    IdColumnOnlyIndex?: undefined;
                                                    SchemaColumnAllocationSize?: undefined;
                                                    SchemaColumnIsGenerated?: undefined;
                                                    SQLDataType?: undefined;
                                                    SchemaPropertyColumn?: undefined;
                                                    SchemaRelationColumn?: undefined;
                                                };
                                                importMapByModulePath: {
                                                    "@airport/air-control": any;
                                                    "@airport/ground-control": any;
                                                    "./SchemaEntity": any;
                                                    "./VersionedSchemaObject": any;
                                                    "./SchemaPropertyColumn"?: undefined;
                                                    "./SchemaRelationColumn"?: undefined;
                                                };
                                            };
                                            properties: ({
                                                decorators: {
                                                    name: string;
                                                    values: {
                                                        name: string;
                                                        nullable: boolean;
                                                    }[];
                                                }[];
                                                isGenerated: boolean;
                                                isId: boolean;
                                                isMappedSuperclass: boolean;
                                                isTransient: boolean;
                                                name: string;
                                                type: string;
                                                ownerEntity: any;
                                                nonArrayType: string;
                                                primitive: string;
                                                index: number;
                                                entity?: undefined;
                                            } | {
                                                decorators: {
                                                    name: string;
                                                    values: {
                                                        name: string;
                                                        referencedColumnName: string;
                                                        nullable: boolean;
                                                    }[];
                                                }[];
                                                isGenerated: boolean;
                                                isId: boolean;
                                                isMappedSuperclass: boolean;
                                                isTransient: boolean;
                                                name: string;
                                                type: string;
                                                ownerEntity: any;
                                                nonArrayType: string;
                                                entity: any;
                                                index: number;
                                                primitive?: undefined;
                                            })[];
                                            methodSignatures: any[];
                                            constructors: {
                                                parameters: any[];
                                                returnType: string;
                                            }[];
                                        };
                                        implementedInterfaceNames: any[];
                                        parentEntity: any;
                                    };
                                    index: number;
                                    primitive?: undefined;
                                    isMap?: undefined;
                                    mapValueType?: undefined;
                                    mapValueIsPrimitive?: undefined;
                                    mapKeyName?: undefined;
                                    mapKeyType?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: any[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    isMap: boolean;
                                    mapValueType: string;
                                    mapValueIsPrimitive: boolean;
                                    mapKeyName: string;
                                    mapKeyType: string;
                                    primitive?: undefined;
                                    index?: undefined;
                                    entity?: undefined;
                                    isArray?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: any[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    isArray: boolean;
                                    nonArrayType: string;
                                    primitive?: undefined;
                                    index?: undefined;
                                    entity?: undefined;
                                    isMap?: undefined;
                                    mapValueType?: undefined;
                                    mapValueIsPrimitive?: undefined;
                                    mapKeyName?: undefined;
                                    mapKeyType?: undefined;
                                })[];
                                methodSignatures: any[];
                                constructors: {
                                    parameters: any[];
                                    returnType: string;
                                }[];
                            };
                            implementedInterfaceNames: any[];
                            parentEntity: any;
                        };
                        index: number;
                        allocationSize?: undefined;
                        primitive?: undefined;
                        isMap?: undefined;
                        mapValueType?: undefined;
                        mapValueIsPrimitive?: undefined;
                        mapKeyName?: undefined;
                        mapKeyType?: undefined;
                    } | {
                        decorators: {
                            name: string;
                            values: {
                                mappedBy: string;
                            }[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        isArray: boolean;
                        nonArrayType: string;
                        entity: {
                            type: string;
                            path: string;
                            parentClassName: string;
                            location: string;
                            isSuperclass: boolean;
                            ids: {
                                decorators: {
                                    name: string;
                                    values: {
                                        name: string;
                                        referencedColumnName: string;
                                        nullable: boolean;
                                    }[];
                                }[];
                                isGenerated: boolean;
                                isId: boolean;
                                isMappedSuperclass: boolean;
                                isTransient: boolean;
                                name: string;
                                type: string;
                                ownerEntity: any;
                                nonArrayType: string;
                                entity: any;
                                index: number;
                            }[];
                            docEntry: {
                                decorators: {
                                    name: string;
                                    values: {
                                        name: string;
                                    }[];
                                }[];
                                isGenerated: boolean;
                                isId: boolean;
                                isMappedSuperclass: boolean;
                                isTransient: boolean;
                                name: string;
                                type: string;
                                fileImports: {
                                    importMapByObjectAsName: {
                                        Column: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                Column: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbNumber: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Entity: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Id: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                JoinColumn: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                ManyToOne: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                Table: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                                DbString?: undefined;
                                                OneToMany?: undefined;
                                                DbBoolean?: undefined;
                                                Json?: undefined;
                                                TableConfiguration?: undefined;
                                                Transient?: undefined;
                                            };
                                            path: string;
                                        };
                                        DbNumber: any;
                                        Entity: any;
                                        Id: any;
                                        JoinColumn: any;
                                        ManyToOne: any;
                                        Table: any;
                                        SchemaReferenceIndex: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaReferenceIndex: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        SchemaVersion: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                SchemaVersion: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        VersionedSchemaObject: {
                                            fileImports: any;
                                            isLocal: boolean;
                                            objectMapByAsName: {
                                                VersionedSchemaObject: {
                                                    asName: string;
                                                    moduleImport: any;
                                                    sourceName: string;
                                                };
                                            };
                                            path: string;
                                        };
                                        DbString?: undefined;
                                        OneToMany?: undefined;
                                        PackageName?: undefined;
                                        SchemaIndex?: undefined;
                                        SchemaName?: undefined;
                                        SchemaScope?: undefined;
                                        SchemaStatus?: undefined;
                                        Domain?: undefined;
                                        DbBoolean?: undefined;
                                        Json?: undefined;
                                        TableConfiguration?: undefined;
                                        Transient?: undefined;
                                        EntityId?: undefined;
                                        EntityIsLocal?: undefined;
                                        EntityIsRepositoryEntity?: undefined;
                                        EntityName?: undefined;
                                        TableIndex?: undefined;
                                        SchemaColumn?: undefined;
                                        SchemaOperation?: undefined;
                                        SchemaProperty?: undefined;
                                        SchemaRelation?: undefined;
                                        ISchemaColumn?: undefined;
                                        ISchemaProperty?: undefined;
                                    };
                                    importMapByModulePath: {
                                        "@airport/air-control": any;
                                        "@airport/ground-control": any;
                                        "./SchemaVersion": any;
                                        "./VersionedSchemaObject": any;
                                        "@airport/territory"?: undefined;
                                        "./SchemaColumn"?: undefined;
                                        "./SchemaOperation"?: undefined;
                                        "./SchemaProperty"?: undefined;
                                        "./SchemaRelation"?: undefined;
                                        "../../generated/schema/schemacolumn"?: undefined;
                                        "../../generated/schema/schemaproperty"?: undefined;
                                    };
                                };
                                properties: ({
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            referencedColumnName: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    entity: any;
                                    index: number;
                                    primitive?: undefined;
                                } | {
                                    decorators: {
                                        name: string;
                                        values: {
                                            name: string;
                                            nullable: boolean;
                                        }[];
                                    }[];
                                    isGenerated: boolean;
                                    isId: boolean;
                                    isMappedSuperclass: boolean;
                                    isTransient: boolean;
                                    name: string;
                                    type: string;
                                    ownerEntity: any;
                                    nonArrayType: string;
                                    primitive: string;
                                    index: number;
                                    entity?: undefined;
                                })[];
                                methodSignatures: any[];
                                constructors: {
                                    parameters: any[];
                                    returnType: string;
                                }[];
                            };
                            implementedInterfaceNames: any[];
                            parentEntity: any;
                        };
                        index: number;
                        allocationSize?: undefined;
                        primitive?: undefined;
                        isMap?: undefined;
                        mapValueType?: undefined;
                        mapValueIsPrimitive?: undefined;
                        mapKeyName?: undefined;
                        mapKeyType?: undefined;
                    } | {
                        decorators: {
                            name: string;
                            values: any[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        nonArrayType: string;
                        isMap: boolean;
                        mapValueType: string;
                        mapValueIsPrimitive: boolean;
                        mapKeyName: string;
                        mapKeyType: string;
                        allocationSize?: undefined;
                        primitive?: undefined;
                        index?: undefined;
                        entity?: undefined;
                        isArray?: undefined;
                    })[];
                    methodSignatures: any[];
                    constructors: {
                        parameters: any[];
                        returnType: string;
                    }[];
                };
                implementedInterfaceNames: any[];
            };
            index: number;
        } | {
            decorators: {
                name: string;
                values: {
                    name: string;
                    referencedColumnName: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            entity: any;
            index: number;
        })[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    project: string;
}[];
//# sourceMappingURL=mappedSuperclass.d.ts.map
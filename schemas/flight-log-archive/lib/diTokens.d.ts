import { IDailySyncLogDao, IMonthlySyncLogDao } from './dao/dao';
export declare const DAILY_SYNC_LOG_DAO: import("@airport/di").DiToken<IDailySyncLogDao>;
export declare const MONTHLY_SYNC_LOG_DAO: import("@airport/di").DiToken<IMonthlySyncLogDao>;

import { IDailySyncLogDao, IMonthlySyncLogDao } from './dao/dao';
export declare const DAILY_SYNC_LOG_DAO: import("@airport/di").IDiToken<IDailySyncLogDao>;
export declare const MONTHLY_SYNC_LOG_DAO: import("@airport/di").IDiToken<IMonthlySyncLogDao>;
//# sourceMappingURL=tokens.d.ts.map
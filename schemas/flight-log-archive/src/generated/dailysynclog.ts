


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IDailySyncLog {
	
	// Id Properties
	databaseId: number;
	date: number;
	repositoryId: number;

	// Id Relations

	// Non-Id Properties

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



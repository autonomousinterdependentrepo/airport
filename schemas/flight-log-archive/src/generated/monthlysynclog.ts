


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IMonthlySyncLog {
	
	// Id Properties
	databaseId: number;
	month: Date;
	repositoryId: number;

	// Id Relations

	// Non-Id Properties
	synced?: boolean;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}






//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ISecurityQuestion {
	
	// Id Properties
	id: number;

	// Id Relations

	// Non-Id Properties
	question?: string;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



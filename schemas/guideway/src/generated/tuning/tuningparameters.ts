


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ITuningParameters {
	
	// Id Properties
	serverType: string;
	parameterGroup: string;
	parameterName: string;

	// Id Relations

	// Non-Id Properties
	parameterValue?: string;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



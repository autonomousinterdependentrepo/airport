


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IArchive {
	
	// Id Properties
	id: string;

	// Id Relations

	// Non-Id Properties
	location?: string;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



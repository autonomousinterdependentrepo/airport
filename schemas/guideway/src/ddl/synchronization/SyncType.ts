export enum SyncType {
	REALTIME,
	RECENT,
	ARCHIVE,
}
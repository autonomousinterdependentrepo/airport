export enum RepositoryStatus {
	ACTIVE,
	ON_HOLD,
	CLOSED,
}
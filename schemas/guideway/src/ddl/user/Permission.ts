export enum UserRepositoryPermission {
	NONE,
	READ,
	WRITE,
	MANAGE
}
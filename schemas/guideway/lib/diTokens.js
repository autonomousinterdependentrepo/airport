"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("@airport/di");
exports.DAILY_ARCHIVE_LOG_DAO = di_1.diToken();
exports.TERMINAL_DAO = di_1.diToken();
exports.TERMINAL_REPOSITORY_DAO = di_1.diToken();
exports.REPOSITORY_DAO = di_1.diToken();
exports.SYNC_LOG_DAO = di_1.diToken();
exports.AGT_SHARING_MESSAGE_DAO = di_1.diToken();
exports.AGT_REPO_TRANS_BLOCK_DAO = di_1.diToken();
//# sourceMappingURL=diTokens.js.map
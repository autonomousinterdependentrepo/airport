import { Repository } from "../repository/Repository";
export declare type MonthlyArchiveLogMonth = number;
export declare type MonthlyArchiveLogNumberOfChanges = number;
export declare class MonthlyArchiveLog {
    repository: Repository;
    monthNumber: MonthlyArchiveLogMonth;
    numberOfChanges: MonthlyArchiveLogNumberOfChanges;
    daysWithChanges: any;
}
//# sourceMappingURL=MonthlyArchiveLog.d.ts.map
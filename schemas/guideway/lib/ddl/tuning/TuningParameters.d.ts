export declare type TuningParametersParameterGroup = string;
export declare type TuningParametersParameterName = string;
export declare type TuningParametersParameterValue = string;
export declare type TuningParametersServerType = string;
export declare class TuningParameters {
    serverType: TuningParametersServerType;
    parameterGroup: TuningParametersParameterGroup;
    parameterName: TuningParametersParameterName;
    parameterValue: TuningParametersParameterValue;
}
//# sourceMappingURL=TuningParameters.d.ts.map
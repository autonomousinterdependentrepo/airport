export var UserRepositoryPermission;
(function (UserRepositoryPermission) {
    UserRepositoryPermission[UserRepositoryPermission["NONE"] = 0] = "NONE";
    UserRepositoryPermission[UserRepositoryPermission["READ"] = 1] = "READ";
    UserRepositoryPermission[UserRepositoryPermission["WRITE"] = 2] = "WRITE";
    UserRepositoryPermission[UserRepositoryPermission["MANAGE"] = 3] = "MANAGE";
})(UserRepositoryPermission || (UserRepositoryPermission = {}));
//# sourceMappingURL=Permission.js.map
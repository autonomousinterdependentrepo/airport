export declare type SecurityQuestionId = number;
export declare class SecurityQuestion {
    id: SecurityQuestionId;
    question: string;
}
//# sourceMappingURL=SecurityQuestion.d.ts.map
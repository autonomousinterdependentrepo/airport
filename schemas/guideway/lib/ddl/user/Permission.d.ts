export declare enum UserRepositoryPermission {
    NONE = 0,
    READ = 1,
    WRITE = 2,
    MANAGE = 3
}
//# sourceMappingURL=Permission.d.ts.map
export declare enum SyncType {
    REALTIME = 0,
    RECENT = 1,
    ARCHIVE = 2
}
//# sourceMappingURL=SyncType.d.ts.map
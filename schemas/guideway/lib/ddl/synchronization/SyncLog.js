var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Entity, Id, JoinColumn, ManyToOne, Table } from "@airport/air-control";
/**
 * A record of syncing a particular record to a particular terminal.
 */
let SyncLog = 
// TODO: partition by add RepositoryTransactionBlockAddDatetime
class SyncLog {
};
__decorate([
    Id(),
    ManyToOne(),
    JoinColumn({ name: "AGT_SHARING_MESSAGE_ID", referencedColumnName: 'ID' })
], SyncLog.prototype, "sharingMessage", void 0);
__decorate([
    Id(),
    ManyToOne(),
    JoinColumn({ name: "AGT_REPO_TRANS_BLOCK_ID", referencedColumnName: 'ID' })
], SyncLog.prototype, "repositoryTransactionBlock", void 0);
SyncLog = __decorate([
    Entity(),
    Table({ name: "AGT_SYNC_LOG" })
    // TODO: partition by add RepositoryTransactionBlockAddDatetime
], SyncLog);
export { SyncLog };
//# sourceMappingURL=SyncLog.js.map
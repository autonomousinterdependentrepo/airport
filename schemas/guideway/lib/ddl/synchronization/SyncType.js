export var SyncType;
(function (SyncType) {
    SyncType[SyncType["REALTIME"] = 0] = "REALTIME";
    SyncType[SyncType["RECENT"] = 1] = "RECENT";
    SyncType[SyncType["ARCHIVE"] = 2] = "ARCHIVE";
})(SyncType || (SyncType = {}));
//# sourceMappingURL=SyncType.js.map
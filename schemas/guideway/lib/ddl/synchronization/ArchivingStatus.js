export var ArchivingStatus;
(function (ArchivingStatus) {
    ArchivingStatus[ArchivingStatus["NOT_ARCHIVING"] = 0] = "NOT_ARCHIVING";
    ArchivingStatus[ArchivingStatus["ARCHIVING_IN_PROGRESS"] = 1] = "ARCHIVING_IN_PROGRESS";
    ArchivingStatus[ArchivingStatus["ARCHIVING_COMPLETE"] = 2] = "ARCHIVING_COMPLETE";
})(ArchivingStatus || (ArchivingStatus = {}));
//# sourceMappingURL=ArchivingStatus.js.map
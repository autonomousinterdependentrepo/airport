export declare enum ArchivingStatus {
    NOT_ARCHIVING = 0,
    ARCHIVING_IN_PROGRESS = 1,
    ARCHIVING_COMPLETE = 2
}
//# sourceMappingURL=ArchivingStatus.d.ts.map
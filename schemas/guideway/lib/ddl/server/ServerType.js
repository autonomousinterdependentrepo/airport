export var ServerType;
(function (ServerType) {
    ServerType[ServerType["REALTIME_SYNCHRONIZER"] = 0] = "REALTIME_SYNCHRONIZER";
    ServerType[ServerType["SYNC_LOG_PROCESSOR"] = 1] = "SYNC_LOG_PROCESSOR";
    ServerType[ServerType["TRANS_DATA_ACCUMULATOR"] = 2] = "TRANS_DATA_ACCUMULATOR";
})(ServerType || (ServerType = {}));
//# sourceMappingURL=ServerType.js.map
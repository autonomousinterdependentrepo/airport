export declare enum ServerType {
    REALTIME_SYNCHRONIZER = 0,
    SYNC_LOG_PROCESSOR = 1,
    TRANS_DATA_ACCUMULATOR = 2
}
//# sourceMappingURL=ServerType.d.ts.map
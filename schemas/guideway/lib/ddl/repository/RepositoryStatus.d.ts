export declare enum RepositoryStatus {
    ACTIVE = 0,
    ON_HOLD = 1,
    CLOSED = 2
}
//# sourceMappingURL=RepositoryStatus.d.ts.map
export declare type ArchiveId = string;
export declare type ArchiveLocation = string;
export declare class Archive {
    id: ArchiveId;
    location: ArchiveLocation;
}
//# sourceMappingURL=Archive.d.ts.map
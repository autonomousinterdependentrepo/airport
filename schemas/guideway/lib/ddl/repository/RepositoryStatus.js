export var RepositoryStatus;
(function (RepositoryStatus) {
    RepositoryStatus[RepositoryStatus["ACTIVE"] = 0] = "ACTIVE";
    RepositoryStatus[RepositoryStatus["ON_HOLD"] = 1] = "ON_HOLD";
    RepositoryStatus[RepositoryStatus["CLOSED"] = 2] = "CLOSED";
})(RepositoryStatus || (RepositoryStatus = {}));
//# sourceMappingURL=RepositoryStatus.js.map
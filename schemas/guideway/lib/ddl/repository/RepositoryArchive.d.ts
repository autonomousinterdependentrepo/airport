import { Archive } from "./Archive";
import { Repository } from "./Repository";
export declare class RepositoryArchive {
    repository: Repository;
    archive: Archive;
}
//# sourceMappingURL=RepositoryArchive.d.ts.map
import { IEntityIdProperties, IEntityCascadeGraph, IEntityUpdateColumns, IEntityUpdateProperties, IEntitySelectProperties, IQStringField, IQEntity, IQRelation } from '@airport/air-control';
import { UserGraph, UserEId, UserEOptionalId, UserESelect, QUserQId, QUserQRelation } from '../quser';
import { SecurityQuestionGraph, SecurityQuestionEId, SecurityQuestionEOptionalId, SecurityQuestionESelect, QSecurityQuestionQId, QSecurityQuestionQRelation } from './qsecurityquestion';
import { SecurityAnswer } from '../../../ddl/user/security/SecurityAnswer';
/**
 * SELECT - All fields and relations (optional).
 */
export interface SecurityAnswerESelect extends IEntitySelectProperties, SecurityAnswerEOptionalId {
    answer?: string | IQStringField;
    user?: UserESelect;
    securityQuestion?: SecurityQuestionESelect;
}
/**
 * DELETE - Ids fields and relations only (required).
 */
export interface SecurityAnswerEId extends IEntityIdProperties {
    user: UserEId;
    securityQuestion: SecurityQuestionEId;
}
/**
 * Ids fields and relations only (optional).
 */
export interface SecurityAnswerEOptionalId {
    user?: UserEOptionalId;
    securityQuestion?: SecurityQuestionEOptionalId;
}
/**
 * UPDATE - non-id fields and relations (optional).
 */
export interface SecurityAnswerEUpdateProperties extends IEntityUpdateProperties {
    answer?: string | IQStringField;
}
/**
 * PERSIST CASCADE - non-id relations (optional).
 */
export interface SecurityAnswerGraph extends SecurityAnswerEOptionalId, IEntityCascadeGraph {
    answer?: string | IQStringField;
    user?: UserGraph;
    securityQuestion?: SecurityQuestionGraph;
}
/**
 * UPDATE - non-id columns (optional).
 */
export interface SecurityAnswerEUpdateColumns extends IEntityUpdateColumns {
    ANSWER?: string | IQStringField;
}
/**
 * CREATE - id fields and relations (required) and non-id fields and relations (optional).
 */
export interface SecurityAnswerECreateProperties extends Partial<SecurityAnswerEId>, SecurityAnswerEUpdateProperties {
}
/**
 * CREATE - id columns (required) and non-id columns (optional).
 */
export interface SecurityAnswerECreateColumns extends SecurityAnswerEId, SecurityAnswerEUpdateColumns {
}
/**
 * Query Entity Query Definition (used for Q.EntityName).
 */
export interface QSecurityAnswer extends IQEntity<SecurityAnswer> {
    user: QUserQRelation;
    securityQuestion: QSecurityQuestionQRelation;
    answer: IQStringField;
}
export interface QSecurityAnswerQId {
    user: QUserQId;
    securityQuestion: QSecurityQuestionQId;
}
export interface QSecurityAnswerQRelation extends IQRelation<SecurityAnswer, QSecurityAnswer>, QSecurityAnswerQId {
}
//# sourceMappingURL=qsecurityanswer.d.ts.map
export interface ITuningParameters {
    serverType: string;
    parameterGroup: string;
    parameterName: string;
    parameterValue?: string;
}
//# sourceMappingURL=tuningparameters.d.ts.map
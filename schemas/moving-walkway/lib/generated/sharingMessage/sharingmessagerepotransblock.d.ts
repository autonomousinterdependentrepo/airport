import { ISharingMessage } from './sharingmessage';
import { IRepositoryTransactionBlock } from '../repositoryTransactionBlock/repositorytransactionblock';
export interface ISharingMessageRepoTransBlock {
    sharingMessage: ISharingMessage;
    repositoryTransactionBlock: IRepositoryTransactionBlock;
}
//# sourceMappingURL=sharingmessagerepotransblock.d.ts.map
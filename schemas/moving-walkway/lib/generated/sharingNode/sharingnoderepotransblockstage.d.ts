export interface ISharingNodeRepoTransBlockStage {
    sharingNodeId: number;
    repositoryTransactionBlockId: number;
    syncStatus?: number;
}
//# sourceMappingURL=sharingnoderepotransblockstage.d.ts.map
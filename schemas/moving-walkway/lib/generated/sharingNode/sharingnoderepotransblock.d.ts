import { ISharingNode } from './sharingnode';
import { IRepositoryTransactionBlock } from '../repositoryTransactionBlock/repositorytransactionblock';
export interface ISharingNodeRepoTransBlock {
    sharingNode: ISharingNode;
    repositoryTransactionBlock: IRepositoryTransactionBlock;
    syncStatus?: number;
}
//# sourceMappingURL=sharingnoderepotransblock.d.ts.map
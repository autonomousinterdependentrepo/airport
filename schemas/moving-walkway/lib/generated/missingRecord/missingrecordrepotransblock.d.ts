import { IMissingRecord } from './missingrecord';
import { IRepositoryTransactionBlock } from '../repositoryTransactionBlock/repositorytransactionblock';
export interface IMissingRecordRepoTransBlock {
    missingRecord?: IMissingRecord;
    repositoryTransactionBlock?: IRepositoryTransactionBlock;
}
//# sourceMappingURL=missingrecordrepotransblock.d.ts.map
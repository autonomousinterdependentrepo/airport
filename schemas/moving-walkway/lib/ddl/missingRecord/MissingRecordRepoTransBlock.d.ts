import { RepositoryTransactionBlock } from '../repositoryTransactionBlock/RepositoryTransactionBlock';
import { MissingRecord } from './MissingRecord';
export declare class MissingRecordRepoTransBlock {
    missingRecord: MissingRecord;
    repositoryTransactionBlock: RepositoryTransactionBlock;
}
//# sourceMappingURL=MissingRecordRepoTransBlock.d.ts.map
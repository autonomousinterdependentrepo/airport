export var MissingRecordStatus;
(function (MissingRecordStatus) {
    MissingRecordStatus[MissingRecordStatus["MISSING"] = 0] = "MISSING";
    MissingRecordStatus[MissingRecordStatus["LOADED"] = 1] = "LOADED";
})(MissingRecordStatus || (MissingRecordStatus = {}));
//# sourceMappingURL=MissingRecordStatus.js.map
import { IMissingRecord } from "../../generated/missingRecord/qmissingrecord";
import { ISharingMessage } from "../../generated/sharingMessage/qsharingmessage";
export declare class MissingRecordSharingMessage {
    missingRecord: IMissingRecord;
    sharingMessage: ISharingMessage;
}

export var SynchronizationConflictType;
(function (SynchronizationConflictType) {
    SynchronizationConflictType[SynchronizationConflictType["LOCAL_UPDATE_REMOTELY_DELETED"] = 0] = "LOCAL_UPDATE_REMOTELY_DELETED";
    SynchronizationConflictType[SynchronizationConflictType["REMOTE_CREATE_REMOTELY_DELETED"] = 1] = "REMOTE_CREATE_REMOTELY_DELETED";
    SynchronizationConflictType[SynchronizationConflictType["REMOTE_UPDATE_LOCALLY_DELETED"] = 2] = "REMOTE_UPDATE_LOCALLY_DELETED";
    SynchronizationConflictType[SynchronizationConflictType["REMOTE_UPDATE_LOCALLY_UPDATED"] = 3] = "REMOTE_UPDATE_LOCALLY_UPDATED";
})(SynchronizationConflictType || (SynchronizationConflictType = {}));
//# sourceMappingURL=SynchronizationConflictType.js.map
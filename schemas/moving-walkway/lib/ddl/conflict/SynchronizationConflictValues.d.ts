import { ColumnIndex } from '@airport/ground-control';
import { SynchronizationConflict } from './SynchronizationConflict';
export declare class SynchronizationConflictValues {
    synchronizationConflict: SynchronizationConflict;
    columnIndex: ColumnIndex;
}
//# sourceMappingURL=SynchronizationConflictValues.d.ts.map
export declare enum SynchronizationConflictType {
    LOCAL_UPDATE_REMOTELY_DELETED = 0,
    REMOTE_CREATE_REMOTELY_DELETED = 1,
    REMOTE_UPDATE_LOCALLY_DELETED = 2,
    REMOTE_UPDATE_LOCALLY_UPDATED = 3
}
//# sourceMappingURL=SynchronizationConflictType.d.ts.map
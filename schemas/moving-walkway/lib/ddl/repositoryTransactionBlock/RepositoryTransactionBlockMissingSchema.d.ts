import { Schema } from "@airport/traffic-pattern";
import { RepositoryTransactionBlock } from "./RepositoryTransactionBlock";
export declare class RepoTransBlockMissingSchema {
    repositoryTransactionBlock: RepositoryTransactionBlock;
    schema: Schema;
}

import { RepoTransBlockSyncOutcomeType, TmRepositoryTransactionBlockId } from "@airport/arrivals-n-departures";
export declare class RepoTransBlockResponseStage {
    id: TmRepositoryTransactionBlockId;
    syncOutcomeType: RepoTransBlockSyncOutcomeType;
}
//# sourceMappingURL=RepoTransBlockResponseStage.d.ts.map
import { Schema } from "@airport/traffic-pattern";
import { RepositoryTransactionBlock } from "./RepositoryTransactionBlock";
export declare class RepoTransBlockSchemaToUpgrade {
    repositoryTransactionBlock: RepositoryTransactionBlock;
    schema: Schema;
}

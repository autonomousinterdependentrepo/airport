import { RepositoryTransactionHistoryBlockId, RepositoryTransactionHistoryId } from "@airport/holding-pattern";
export declare class RepositoryTransactionHistoryUpdateStage {
    repositoryTransactionHistoryId: RepositoryTransactionHistoryId;
    blockId: RepositoryTransactionHistoryBlockId;
}
//# sourceMappingURL=RepositoryTransactionHistoryUpdateStage.d.ts.map
export var SharingNodeProtocol;
(function (SharingNodeProtocol) {
    SharingNodeProtocol[SharingNodeProtocol["HTTPS"] = 0] = "HTTPS";
    SharingNodeProtocol[SharingNodeProtocol["LOCAL"] = 1] = "LOCAL";
})(SharingNodeProtocol || (SharingNodeProtocol = {}));
//# sourceMappingURL=SharingNodeProtocol.js.map
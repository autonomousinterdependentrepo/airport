export declare enum SharingMechanism {
    CENTRAL_SERVER = 0,
    FILE_SYSTEM = 1,
    PEER_TO_PEER = 2
}
//# sourceMappingURL=SharingMechanism.d.ts.map
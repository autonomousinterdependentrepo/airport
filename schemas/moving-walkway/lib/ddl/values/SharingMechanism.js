export var SharingMechanism;
(function (SharingMechanism) {
    SharingMechanism[SharingMechanism["CENTRAL_SERVER"] = 0] = "CENTRAL_SERVER";
    SharingMechanism[SharingMechanism["FILE_SYSTEM"] = 1] = "FILE_SYSTEM";
    SharingMechanism[SharingMechanism["PEER_TO_PEER"] = 2] = "PEER_TO_PEER";
})(SharingMechanism || (SharingMechanism = {}));
//# sourceMappingURL=SharingMechanism.js.map
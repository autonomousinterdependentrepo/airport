export declare enum SchemaChangeStatus {
    CHANGE_NEEDED = 0,
    CHANGE_COMPLETED = 1
}
//# sourceMappingURL=SchemaChangeStatus.d.ts.map
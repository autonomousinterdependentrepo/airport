export var SchemaChangeStatus;
(function (SchemaChangeStatus) {
    SchemaChangeStatus[SchemaChangeStatus["CHANGE_NEEDED"] = 0] = "CHANGE_NEEDED";
    SchemaChangeStatus[SchemaChangeStatus["CHANGE_COMPLETED"] = 1] = "CHANGE_COMPLETED";
})(SchemaChangeStatus || (SchemaChangeStatus = {}));
//# sourceMappingURL=SchemaChangeStatus.js.map
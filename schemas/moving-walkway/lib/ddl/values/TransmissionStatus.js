export var TransmissionStatus;
(function (TransmissionStatus) {
    TransmissionStatus[TransmissionStatus["ACKNOWLEDGED"] = 0] = "ACKNOWLEDGED";
    TransmissionStatus[TransmissionStatus["SENT"] = 1] = "SENT";
})(TransmissionStatus || (TransmissionStatus = {}));
//# sourceMappingURL=TransmissionStatus.js.map
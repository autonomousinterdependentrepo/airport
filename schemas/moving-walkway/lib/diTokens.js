"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("@airport/di");
exports.MISSING_RECORD_DAO = di_1.diToken();
exports.MISSING_RECORD_REPO_TRANS_BLOCK_DAO = di_1.diToken();
exports.RECORD_UPDATE_STAGE_DAO = di_1.diToken();
exports.REPO_TRANS_BLOCK_DAO = di_1.diToken();
exports.REPO_TRANS_BLOCK_DUO = di_1.diToken();
exports.REPO_TRANS_HISTORY_UPDATE_STAGE_DAO = di_1.diToken();
exports.REPO_TRANS_BLOCK_RESPONSE_STAGE_DAO = di_1.diToken();
exports.SHARING_MESSAGE_DAO = di_1.diToken();
exports.SHARING_MESSAGE_REPO_TRANS_BLOCK_DAO = di_1.diToken();
// export const SharingMessageResponseStageDaoToken
// 	= diToken<ISharingMessageResponseStageDao>();
exports.REPO_TRANS_BLOCK_SCHEMA_TO_CHANGE_DAO = di_1.diToken();
exports.SHARING_NODE_DAO = di_1.diToken();
exports.SHARING_NODE_TERMINAL_DAO = di_1.diToken();
exports.SHARING_NODE_REPOSITORY_DAO = di_1.diToken();
exports.SHARING_NODE_REPO_TRANS_BLOCK_DAO = di_1.diToken();
exports.SHARING_NODE_REPO_TRANS_BLOCK_STAGE_DAO = di_1.diToken();
exports.SYNC_CONFLICT_DAO = di_1.diToken();
exports.SYNC_CONFLICT_PENDING_NOTIFICATION_DAO = di_1.diToken();
exports.SYNC_CONFLICT_VALUES_DAO = di_1.diToken();
//# sourceMappingURL=diTokens.js.map
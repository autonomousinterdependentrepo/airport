import { DI } from '@airport/di';
import { REPO_TRANS_BLOCK_SCHEMA_TO_CHANGE_DAO } from '../../tokens';
import { BaseRepoTransBlockSchemaToChangeDao } from '../../generated/generated';
export class RepoTransBlockSchemaToChangeDao extends BaseRepoTransBlockSchemaToChangeDao {
}
DI.set(REPO_TRANS_BLOCK_SCHEMA_TO_CHANGE_DAO, RepoTransBlockSchemaToChangeDao);
//# sourceMappingURL=RepoTransBlockSchemaToChangeDao.js.map
import {
	IMissingRecord,
} from './missingrecord';
import {
	IRepositoryTransactionBlock,
} from '../repositoryTransactionBlock/repositorytransactionblock';



//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IMissingRecordRepoTransBlock {
	
	// Id Properties

	// Id Relations

	// Non-Id Properties

	// Non-Id Relations
	missingRecord?: IMissingRecord;
	repositoryTransactionBlock?: IRepositoryTransactionBlock;

	// Transient Properties

	// Public Methods
	
}



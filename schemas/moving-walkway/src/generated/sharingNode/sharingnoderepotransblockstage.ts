


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ISharingNodeRepoTransBlockStage {
	
	// Id Properties
	sharingNodeId: number;
	repositoryTransactionBlockId: number;

	// Id Relations

	// Non-Id Properties
	syncStatus?: number;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



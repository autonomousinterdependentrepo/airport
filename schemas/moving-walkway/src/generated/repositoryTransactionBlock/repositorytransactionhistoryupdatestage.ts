


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IRepositoryTransactionHistoryUpdateStage {
	
	// Id Properties
	repositoryTransactionHistoryId: number;

	// Id Relations

	// Non-Id Properties
	blockId?: number;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



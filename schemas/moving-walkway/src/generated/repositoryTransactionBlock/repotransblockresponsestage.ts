


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IRepoTransBlockResponseStage {
	
	// Id Properties
	id: number;

	// Id Relations

	// Non-Id Properties
	syncOutcomeType?: number;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



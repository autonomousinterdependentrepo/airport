export enum SharingMechanism {
	CENTRAL_SERVER,
	FILE_SYSTEM,
	PEER_TO_PEER,
}
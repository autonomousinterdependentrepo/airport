export enum SchemaChangeStatus {
	CHANGE_NEEDED,
	CHANGE_COMPLETED
}
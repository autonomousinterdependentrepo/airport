export enum SynchronizationConflictType {
	LOCAL_UPDATE_REMOTELY_DELETED,
	REMOTE_CREATE_REMOTELY_DELETED,
	REMOTE_UPDATE_LOCALLY_DELETED,
	REMOTE_UPDATE_LOCALLY_UPDATED,
}
import { ApplicationPackageId } from '@airport/ground-control';
import { Application } from "./Application";
import { Package } from "./Package";
export declare class ApplicationPackage {
    id: ApplicationPackageId;
    application: Application;
    package: Package;
}
//# sourceMappingURL=ApplicationPackage.d.ts.map
import { Package } from "./Package";
export declare type PackagedUnitId = number;
export declare type PackagedUnitName = string;
export declare class PackagedUnit {
    id: PackagedUnitId;
    name: PackagedUnitName;
    package: Package;
}
//# sourceMappingURL=PackagedUnit.d.ts.map
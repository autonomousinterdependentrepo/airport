"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("@airport/di");
exports.APPLICATION_DAO = di_1.diToken();
exports.APPLICATION_PACKAGE_DAO = di_1.diToken();
exports.DOMAIN_DAO = di_1.diToken();
exports.PACKAGE_DAO = di_1.diToken();
exports.PACKAGE_UNIT_DAO = di_1.diToken();
//# sourceMappingURL=diTokens.js.map
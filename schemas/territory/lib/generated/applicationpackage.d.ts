import { IApplication } from './application';
import { IPackage } from './package';
export interface IApplicationPackage {
    id: number;
    application?: IApplication;
    package?: IPackage;
}
//# sourceMappingURL=applicationpackage.d.ts.map
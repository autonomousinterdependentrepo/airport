import { IPackage } from './package';
export interface IPackagedUnit {
    id: number;
    name?: string;
    package?: IPackage;
}
//# sourceMappingURL=packagedunit.d.ts.map
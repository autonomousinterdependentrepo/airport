import { IApplicationDao } from './dao/ApplicationDao';
import { IApplicationPackageDao } from './dao/ApplicationPackageDao';
import { IDomainDao } from './dao/DomainDao';
import { IPackageDao } from './dao/PackageDao';
import { IPackagedUnitDao } from './dao/PackagedUnitDao';
export declare const APPLICATION_DAO: import("@airport/di").IDiToken<IApplicationDao>;
export declare const APPLICATION_PACKAGE_DAO: import("@airport/di").IDiToken<IApplicationPackageDao>;
export declare const DOMAIN_DAO: import("@airport/di").IDiToken<IDomainDao>;
export declare const PACKAGE_DAO: import("@airport/di").IDiToken<IPackageDao>;
export declare const PACKAGE_UNIT_DAO: import("@airport/di").IDiToken<IPackagedUnitDao>;
//# sourceMappingURL=tokens.d.ts.map
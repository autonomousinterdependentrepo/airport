import {
	IQEntityInternal,
	IEntityIdProperties,
	IEntityCascadeGraph,
	IEntityUpdateColumns,
	IEntityUpdateProperties,
	IEntitySelectProperties,
	IEntityDatabaseFacade,
	IEntityFind,
	IEntityFindOne,
	IEntitySearch,
	IEntitySearchOne,
	IQBooleanField,
	IQDateField,
	IQNumberField,
	IQOneToManyRelation,
	IQStringField,
	IQUntypedField,
	IQEntity,
	IQRelation,
	RawDelete,
	RawUpdate,
} from '@airport/air-control';
import {
	PackageGraph,
	PackageEId,
	PackageEOptionalId,
	PackageEUpdateProperties,
	PackageESelect,
	QPackage,
	QPackageQId,
	QPackageQRelation,
} from './qpackage';
import {
	Package,
} from '../ddl/Package';
import {
	PackagedUnit,
} from '../ddl/PackagedUnit';


declare function require(moduleName: string): any;


//////////////////////////////
//  API SPECIFIC INTERFACES //
//////////////////////////////

/**
 * SELECT - All fields and relations (optional).
 */
export interface PackagedUnitESelect
    extends IEntitySelectProperties, PackagedUnitEOptionalId {
	// Non-Id Properties
	name?: string | IQStringField;

	// Id Relations - full property interfaces

  // Non-Id relations (including OneToMany's)
	package?: PackageESelect;

}

/**
 * DELETE - Ids fields and relations only (required).
 */
export interface PackagedUnitEId
    extends IEntityIdProperties {
	// Id Properties
	id: number | IQNumberField;

	// Id Relations - Ids only

}

/**
 * Ids fields and relations only (optional).
 */
export interface PackagedUnitEOptionalId {
	// Id Properties
	id?: number | IQNumberField;

	// Id Relations - Ids only

}

/**
 * UPDATE - non-id fields and relations (optional).
 */
export interface PackagedUnitEUpdateProperties
	extends IEntityUpdateProperties {
	// Non-Id Properties
	name?: string | IQStringField;

	// Non-Id Relations - ids only & no OneToMany's
	package?: PackageEOptionalId;

}

/**
 * PERSIST CASCADE - non-id relations (optional).
 */
export interface PackagedUnitGraph
	extends PackagedUnitEOptionalId, IEntityCascadeGraph {
// NOT USED: Cascading Relations
// NOT USED: ${relationsForCascadeGraph}
	// Non-Id Properties
	name?: string | IQStringField;

	// Relations
	package?: PackageGraph;

}

/**
 * UPDATE - non-id columns (optional).
 */
export interface PackagedUnitEUpdateColumns
	extends IEntityUpdateColumns {
	// Non-Id Columns
	NAME?: string | IQStringField;
	PACKAGE_ID?: number | IQNumberField;

}

/**
 * CREATE - id fields and relations (required) and non-id fields and relations (optional).
 */
export interface PackagedUnitECreateProperties
extends Partial<PackagedUnitEId>, PackagedUnitEUpdateProperties {
}

/**
 * CREATE - id columns (required) and non-id columns (optional).
 */
export interface PackagedUnitECreateColumns
extends PackagedUnitEId, PackagedUnitEUpdateColumns {
}




///////////////////////////////////////////////
//  QUERY IMPLEMENTATION SPECIFIC INTERFACES //
///////////////////////////////////////////////

/**
 * Query Entity Query Definition (used for Q.EntityName).
 */
export interface QPackagedUnit extends IQEntity<PackagedUnit>
{
	// Id Fields
	id: IQNumberField;

	// Id Relations

	// Non-Id Fields
	name: IQStringField;

	// Non-Id Relations
	package: QPackageQRelation;

}


// Entity Id Interface
export interface QPackagedUnitQId
{
	
	// Id Fields
	id: IQNumberField;

	// Id Relations


}

// Entity Relation Interface
export interface QPackagedUnitQRelation
	extends IQRelation<PackagedUnit, QPackagedUnit>, QPackagedUnitQId {
}


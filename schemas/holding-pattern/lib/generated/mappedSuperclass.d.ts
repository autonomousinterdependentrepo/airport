export declare const MAPPED_SUPERCLASS: ({
    type: string;
    path: string;
    parentClassName: any;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                Column: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Column: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        GeneratedValue?: undefined;
                        Id?: undefined;
                        JoinColumn?: undefined;
                        ManyToOne?: undefined;
                    };
                    path: string;
                };
                MappedSuperclass: any;
                GeneratedValue?: undefined;
                Id?: undefined;
                JoinColumn?: undefined;
                ManyToOne?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Stageable?: undefined;
                Repository?: undefined;
                RepositoryEntity?: undefined;
                IUser?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "../infrastructure/Stageable"?: undefined;
                "./Repository"?: undefined;
                "../repository/RepositoryEntity"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: {
            decorators: {
                name: string;
                values: {
                    name: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
        }[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    project: string;
    location?: undefined;
    parentEntity?: undefined;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: ({
        decorators: {
            name: string;
            values: {
                name: string;
                referencedColumnName: string;
                nullable: boolean;
            }[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        ownerEntity: any;
        nonArrayType: string;
        entity: any;
        index: number;
        primitive?: undefined;
    } | {
        decorators: {
            name: string;
            values: {
                name: string;
                nullable: boolean;
            }[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        ownerEntity: any;
        nonArrayType: string;
        primitive: string;
        index: number;
        entity?: undefined;
    })[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                Column: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Column: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        GeneratedValue: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        Id: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        JoinColumn: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        ManyToOne: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                GeneratedValue: any;
                Id: any;
                JoinColumn: any;
                ManyToOne: any;
                MappedSuperclass: any;
                Actor: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Actor: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                SystemWideOperationId: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        SystemWideOperationId: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Stageable: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Stageable: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Repository: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Repository: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                RepositoryEntity?: undefined;
                IUser?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "../infrastructure/Actor": any;
                "../common": any;
                "../infrastructure/Stageable": any;
                "./Repository": any;
                "../repository/RepositoryEntity"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: ({
            decorators: {
                name: string;
                values: {
                    name: string;
                    referencedColumnName: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            entity: any;
            index: number;
            primitive?: undefined;
        } | {
            decorators: {
                name: string;
                values: {
                    name: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
            entity?: undefined;
        })[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: any;
        isSuperclass: boolean;
        ids: any[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue?: undefined;
                            Id?: undefined;
                            JoinColumn?: undefined;
                            ManyToOne?: undefined;
                        };
                        path: string;
                    };
                    MappedSuperclass: any;
                    GeneratedValue?: undefined;
                    Id?: undefined;
                    JoinColumn?: undefined;
                    ManyToOne?: undefined;
                    Actor?: undefined;
                    SystemWideOperationId?: undefined;
                    Stageable?: undefined;
                    Repository?: undefined;
                    IUser?: undefined;
                    RepositoryEntity?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "../infrastructure/Actor"?: undefined;
                    "../common"?: undefined;
                    "../infrastructure/Stageable"?: undefined;
                    "./Repository"?: undefined;
                    "@airport/travel-document-checkpoint"?: undefined;
                    "../repository/RepositoryEntity"?: undefined;
                };
            };
            properties: {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
            }[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        project: string;
        location?: undefined;
        parentEntity?: undefined;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                MappedSuperclass: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                RepositoryEntity: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        RepositoryEntity: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Column?: undefined;
                GeneratedValue?: undefined;
                Id?: undefined;
                JoinColumn?: undefined;
                ManyToOne?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Stageable?: undefined;
                Repository?: undefined;
                IUser?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "../repository/RepositoryEntity": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "../infrastructure/Stageable"?: undefined;
                "./Repository"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: any[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: string;
        location: string;
        isSuperclass: boolean;
        ids: ({
            decorators: {
                name: string;
                values: {
                    name: string;
                    referencedColumnName: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            entity: any;
            index: number;
            primitive?: undefined;
        } | {
            decorators: {
                name: string;
                values: {
                    name: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
            entity?: undefined;
        })[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            Id: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            JoinColumn: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            ManyToOne: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    GeneratedValue: any;
                    Id: any;
                    JoinColumn: any;
                    ManyToOne: any;
                    MappedSuperclass: any;
                    Actor: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Actor: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    SystemWideOperationId: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            SystemWideOperationId: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    Stageable: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Stageable: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    Repository: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Repository: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    IUser?: undefined;
                    RepositoryEntity?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "../infrastructure/Actor": any;
                    "../common": any;
                    "../infrastructure/Stageable": any;
                    "./Repository": any;
                    "@airport/travel-document-checkpoint"?: undefined;
                    "../repository/RepositoryEntity"?: undefined;
                };
            };
            properties: ({
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        referencedColumnName: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                entity: any;
                index: number;
                primitive?: undefined;
            } | {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
                entity?: undefined;
            })[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        parentEntity: {
            type: string;
            path: string;
            parentClassName: any;
            isSuperclass: boolean;
            ids: any[];
            docEntry: {
                decorators: {
                    name: string;
                    values: any[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                fileImports: {
                    importMapByObjectAsName: {
                        Column: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Column: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                MappedSuperclass: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                GeneratedValue?: undefined;
                                Id?: undefined;
                                JoinColumn?: undefined;
                                ManyToOne?: undefined;
                            };
                            path: string;
                        };
                        MappedSuperclass: any;
                        GeneratedValue?: undefined;
                        Id?: undefined;
                        JoinColumn?: undefined;
                        ManyToOne?: undefined;
                        Actor?: undefined;
                        SystemWideOperationId?: undefined;
                        Stageable?: undefined;
                        Repository?: undefined;
                    };
                    importMapByModulePath: {
                        "@airport/air-control": any;
                        "../infrastructure/Actor"?: undefined;
                        "../common"?: undefined;
                        "../infrastructure/Stageable"?: undefined;
                        "./Repository"?: undefined;
                    };
                };
                properties: {
                    decorators: {
                        name: string;
                        values: {
                            name: string;
                            nullable: boolean;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    ownerEntity: any;
                    nonArrayType: string;
                    primitive: string;
                    index: number;
                }[];
                methodSignatures: any[];
                constructors: {
                    parameters: any[];
                    returnType: string;
                }[];
            };
            implementedInterfaceNames: any[];
            project: string;
            location?: undefined;
            parentEntity?: undefined;
        };
        project: string;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                MappedSuperclass: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Stageable: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Stageable: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Column?: undefined;
                GeneratedValue?: undefined;
                Id?: undefined;
                JoinColumn?: undefined;
                ManyToOne?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Repository?: undefined;
                RepositoryEntity?: undefined;
                IUser?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "../infrastructure/Stageable": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "./Repository"?: undefined;
                "../repository/RepositoryEntity"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: any[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: any;
        isSuperclass: boolean;
        ids: any[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue?: undefined;
                            Id?: undefined;
                            JoinColumn?: undefined;
                            ManyToOne?: undefined;
                        };
                        path: string;
                    };
                    MappedSuperclass: any;
                    GeneratedValue?: undefined;
                    Id?: undefined;
                    JoinColumn?: undefined;
                    ManyToOne?: undefined;
                    Actor?: undefined;
                    SystemWideOperationId?: undefined;
                    Stageable?: undefined;
                    Repository?: undefined;
                    IUser?: undefined;
                    RepositoryEntity?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "../infrastructure/Actor"?: undefined;
                    "../common"?: undefined;
                    "../infrastructure/Stageable"?: undefined;
                    "./Repository"?: undefined;
                    "@airport/travel-document-checkpoint"?: undefined;
                    "../repository/RepositoryEntity"?: undefined;
                };
            };
            properties: {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
            }[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        project: string;
        location?: undefined;
        parentEntity?: undefined;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                Column: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Column: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        JoinColumn: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        ManyToOne: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        GeneratedValue?: undefined;
                        Id?: undefined;
                    };
                    path: string;
                };
                JoinColumn: any;
                ManyToOne: any;
                MappedSuperclass: any;
                IUser: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        IUser: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                RepositoryEntity: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        RepositoryEntity: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                GeneratedValue?: undefined;
                Id?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Stageable?: undefined;
                Repository?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "@airport/travel-document-checkpoint": any;
                "../repository/RepositoryEntity": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "../infrastructure/Stageable"?: undefined;
                "./Repository"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: {
            decorators: {
                name: string;
                values: {
                    name: string;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
        }[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: string;
        location: string;
        isSuperclass: boolean;
        ids: ({
            decorators: {
                name: string;
                values: {
                    name: string;
                    referencedColumnName: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            entity: any;
            index: number;
            primitive?: undefined;
        } | {
            decorators: {
                name: string;
                values: {
                    name: string;
                    nullable: boolean;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
            entity?: undefined;
        })[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            Id: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            JoinColumn: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            ManyToOne: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    GeneratedValue: any;
                    Id: any;
                    JoinColumn: any;
                    ManyToOne: any;
                    MappedSuperclass: any;
                    Actor: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Actor: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    SystemWideOperationId: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            SystemWideOperationId: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    Stageable: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Stageable: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    Repository: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Repository: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    IUser?: undefined;
                    RepositoryEntity?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "../infrastructure/Actor": any;
                    "../common": any;
                    "../infrastructure/Stageable": any;
                    "./Repository": any;
                    "@airport/travel-document-checkpoint"?: undefined;
                    "../repository/RepositoryEntity"?: undefined;
                };
            };
            properties: ({
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        referencedColumnName: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                entity: any;
                index: number;
                primitive?: undefined;
            } | {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
                entity?: undefined;
            })[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        parentEntity: {
            type: string;
            path: string;
            parentClassName: any;
            isSuperclass: boolean;
            ids: any[];
            docEntry: {
                decorators: {
                    name: string;
                    values: any[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                fileImports: {
                    importMapByObjectAsName: {
                        Column: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Column: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                MappedSuperclass: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                GeneratedValue?: undefined;
                                Id?: undefined;
                                JoinColumn?: undefined;
                                ManyToOne?: undefined;
                            };
                            path: string;
                        };
                        MappedSuperclass: any;
                        GeneratedValue?: undefined;
                        Id?: undefined;
                        JoinColumn?: undefined;
                        ManyToOne?: undefined;
                        Actor?: undefined;
                        SystemWideOperationId?: undefined;
                        Stageable?: undefined;
                        Repository?: undefined;
                    };
                    importMapByModulePath: {
                        "@airport/air-control": any;
                        "../infrastructure/Actor"?: undefined;
                        "../common"?: undefined;
                        "../infrastructure/Stageable"?: undefined;
                        "./Repository"?: undefined;
                    };
                };
                properties: {
                    decorators: {
                        name: string;
                        values: {
                            name: string;
                            nullable: boolean;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    ownerEntity: any;
                    nonArrayType: string;
                    primitive: string;
                    index: number;
                }[];
                methodSignatures: any[];
                constructors: {
                    parameters: any[];
                    returnType: string;
                }[];
            };
            implementedInterfaceNames: any[];
            project: string;
            location?: undefined;
            parentEntity?: undefined;
        };
        project: string;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                Column: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Column: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        JoinColumn: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        ManyToOne: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        GeneratedValue?: undefined;
                        Id?: undefined;
                    };
                    path: string;
                };
                JoinColumn: any;
                ManyToOne: any;
                MappedSuperclass: any;
                IUser: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        IUser: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Stageable: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Stageable: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                GeneratedValue?: undefined;
                Id?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Repository?: undefined;
                RepositoryEntity?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "@airport/travel-document-checkpoint": any;
                "../infrastructure/Stageable": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "./Repository"?: undefined;
                "../repository/RepositoryEntity"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: ({
            decorators: {
                name: string;
                values: {
                    name: string;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            fromProject: string;
            otherSchemaDbEntity: {
                columnMap: any;
                columns: ({
                    entity: any;
                    id: any;
                    index: number;
                    isGenerated: boolean;
                    manyRelationColumns: any[];
                    name: string;
                    notNull: boolean;
                    oneRelationColumns: any[];
                    propertyColumnMap: any;
                    propertyColumns: {
                        column: any;
                        property: any;
                        sinceVersion: any;
                    }[];
                    sinceVersion: any;
                    type: number;
                    idIndex: number;
                } | {
                    entity: any;
                    id: any;
                    index: number;
                    isGenerated: boolean;
                    manyRelationColumns: any[];
                    name: string;
                    notNull: boolean;
                    oneRelationColumns: any[];
                    propertyColumnMap: any;
                    propertyColumns: {
                        column: any;
                        property: any;
                        sinceVersion: any;
                    }[];
                    sinceVersion: any;
                    type: number;
                    idIndex?: undefined;
                })[];
                idColumns: {
                    entity: any;
                    id: any;
                    index: number;
                    isGenerated: boolean;
                    manyRelationColumns: any[];
                    name: string;
                    notNull: boolean;
                    oneRelationColumns: any[];
                    propertyColumnMap: any;
                    propertyColumns: {
                        column: any;
                        property: any;
                        sinceVersion: any;
                    }[];
                    sinceVersion: any;
                    type: number;
                    idIndex: number;
                }[];
                idColumnMap: any;
                id: any;
                index: number;
                isLocal: boolean;
                isRepositoryEntity: boolean;
                name: string;
                propertyMap: any;
                properties: ({
                    propertyColumns: {
                        column: any;
                        property: any;
                        sinceVersion: any;
                    }[];
                    entity: any;
                    id: any;
                    index: number;
                    isId: boolean;
                    name: string;
                    relation: any;
                    sinceVersion: any;
                } | {
                    propertyColumns: any[];
                    entity: any;
                    id: any;
                    index: number;
                    isId: boolean;
                    name: string;
                    relation: {
                        isId: boolean;
                        oneToManyElems: any;
                        relationType: number;
                        id: any;
                        index: number;
                        property: any;
                        manyRelationColumns: any[];
                        oneRelationColumns: any[];
                        relationEntity: any;
                        sinceVersion: any;
                    }[];
                    sinceVersion: any;
                })[];
                relationReferences: any[];
                relations: {
                    isId: boolean;
                    oneToManyElems: any;
                    relationType: number;
                    id: any;
                    index: number;
                    property: any;
                    manyRelationColumns: any[];
                    oneRelationColumns: any[];
                    relationEntity: any;
                    sinceVersion: any;
                }[];
                schemaVersion: any;
                sinceVersion: any;
                tableConfig: any;
            };
            index: number;
            primitive?: undefined;
        } | {
            decorators: {
                name: string;
                values: {
                    name: string;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
            fromProject?: undefined;
            otherSchemaDbEntity?: undefined;
        })[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: any;
        isSuperclass: boolean;
        ids: any[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue?: undefined;
                            Id?: undefined;
                            JoinColumn?: undefined;
                            ManyToOne?: undefined;
                        };
                        path: string;
                    };
                    MappedSuperclass: any;
                    GeneratedValue?: undefined;
                    Id?: undefined;
                    JoinColumn?: undefined;
                    ManyToOne?: undefined;
                    Actor?: undefined;
                    SystemWideOperationId?: undefined;
                    Stageable?: undefined;
                    Repository?: undefined;
                    IUser?: undefined;
                    RepositoryEntity?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "../infrastructure/Actor"?: undefined;
                    "../common"?: undefined;
                    "../infrastructure/Stageable"?: undefined;
                    "./Repository"?: undefined;
                    "@airport/travel-document-checkpoint"?: undefined;
                    "../repository/RepositoryEntity"?: undefined;
                };
            };
            properties: {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
            }[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        project: string;
        location?: undefined;
        parentEntity?: undefined;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                Column: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Column: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        GeneratedValue?: undefined;
                        Id?: undefined;
                        JoinColumn?: undefined;
                        ManyToOne?: undefined;
                    };
                    path: string;
                };
                MappedSuperclass: any;
                ImmutableRepoRow: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        ImmutableRepoRow: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                GeneratedValue?: undefined;
                Id?: undefined;
                JoinColumn?: undefined;
                ManyToOne?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Stageable?: undefined;
                Repository?: undefined;
                RepositoryEntity?: undefined;
                IUser?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "./ImmutableRepoRow": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "../infrastructure/Stageable"?: undefined;
                "./Repository"?: undefined;
                "../repository/RepositoryEntity"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: {
            decorators: {
                name: string;
                values: {
                    name: string;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
        }[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: string;
        location: string;
        isSuperclass: boolean;
        ids: any[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            JoinColumn: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            ManyToOne: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue?: undefined;
                            Id?: undefined;
                        };
                        path: string;
                    };
                    JoinColumn: any;
                    ManyToOne: any;
                    MappedSuperclass: any;
                    IUser: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            IUser: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    RepositoryEntity: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            RepositoryEntity: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    GeneratedValue?: undefined;
                    Id?: undefined;
                    Actor?: undefined;
                    SystemWideOperationId?: undefined;
                    Stageable?: undefined;
                    Repository?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "@airport/travel-document-checkpoint": any;
                    "../repository/RepositoryEntity": any;
                    "../infrastructure/Actor"?: undefined;
                    "../common"?: undefined;
                    "../infrastructure/Stageable"?: undefined;
                    "./Repository"?: undefined;
                };
            };
            properties: {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
            }[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        parentEntity: {
            type: string;
            path: string;
            parentClassName: string;
            location: string;
            isSuperclass: boolean;
            ids: ({
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        referencedColumnName: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                entity: any;
                index: number;
                primitive?: undefined;
            } | {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                        nullable: boolean;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
                entity?: undefined;
            })[];
            docEntry: {
                decorators: {
                    name: string;
                    values: any[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                fileImports: {
                    importMapByObjectAsName: {
                        Column: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Column: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                GeneratedValue: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                Id: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                JoinColumn: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                ManyToOne: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                MappedSuperclass: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                            };
                            path: string;
                        };
                        GeneratedValue: any;
                        Id: any;
                        JoinColumn: any;
                        ManyToOne: any;
                        MappedSuperclass: any;
                        Actor: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Actor: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                            };
                            path: string;
                        };
                        SystemWideOperationId: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                SystemWideOperationId: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                            };
                            path: string;
                        };
                        Stageable: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Stageable: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                            };
                            path: string;
                        };
                        Repository: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Repository: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                            };
                            path: string;
                        };
                    };
                    importMapByModulePath: {
                        "@airport/air-control": any;
                        "../infrastructure/Actor": any;
                        "../common": any;
                        "../infrastructure/Stageable": any;
                        "./Repository": any;
                    };
                };
                properties: ({
                    decorators: {
                        name: string;
                        values: {
                            name: string;
                            referencedColumnName: string;
                            nullable: boolean;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    ownerEntity: any;
                    nonArrayType: string;
                    entity: any;
                    index: number;
                    primitive?: undefined;
                } | {
                    decorators: {
                        name: string;
                        values: {
                            name: string;
                            nullable: boolean;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    ownerEntity: any;
                    nonArrayType: string;
                    primitive: string;
                    index: number;
                    entity?: undefined;
                })[];
                methodSignatures: any[];
                constructors: {
                    parameters: any[];
                    returnType: string;
                }[];
            };
            implementedInterfaceNames: any[];
            parentEntity: {
                type: string;
                path: string;
                parentClassName: any;
                isSuperclass: boolean;
                ids: any[];
                docEntry: {
                    decorators: {
                        name: string;
                        values: any[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    fileImports: {
                        importMapByObjectAsName: {
                            Column: {
                                fileImports: any;
                                isLocal: boolean;
                                objectMapByAsName: {
                                    Column: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                    MappedSuperclass: {
                                        asName: string;
                                        moduleImport: any;
                                        sourceName: string;
                                    };
                                };
                                path: string;
                            };
                            MappedSuperclass: any;
                        };
                        importMapByModulePath: {
                            "@airport/air-control": any;
                        };
                    };
                    properties: {
                        decorators: {
                            name: string;
                            values: {
                                name: string;
                                nullable: boolean;
                            }[];
                        }[];
                        isGenerated: boolean;
                        isId: boolean;
                        isMappedSuperclass: boolean;
                        isTransient: boolean;
                        name: string;
                        type: string;
                        ownerEntity: any;
                        nonArrayType: string;
                        primitive: string;
                        index: number;
                    }[];
                    methodSignatures: any[];
                    constructors: {
                        parameters: any[];
                        returnType: string;
                    }[];
                };
                implementedInterfaceNames: any[];
                project: string;
            };
            project: string;
        };
        project: string;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: string;
    location: string;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                Column: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        Column: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                        GeneratedValue?: undefined;
                        Id?: undefined;
                        JoinColumn?: undefined;
                        ManyToOne?: undefined;
                    };
                    path: string;
                };
                MappedSuperclass: any;
                ImmutableRow: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        ImmutableRow: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                GeneratedValue?: undefined;
                Id?: undefined;
                JoinColumn?: undefined;
                ManyToOne?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Stageable?: undefined;
                Repository?: undefined;
                RepositoryEntity?: undefined;
                IUser?: undefined;
                ImmutableRepoRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "./ImmutableRow": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "../infrastructure/Stageable"?: undefined;
                "./Repository"?: undefined;
                "../repository/RepositoryEntity"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRepoRow"?: undefined;
            };
        };
        properties: {
            decorators: {
                name: string;
                values: {
                    name: string;
                }[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            ownerEntity: any;
            nonArrayType: string;
            primitive: string;
            index: number;
        }[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    parentEntity: {
        type: string;
        path: string;
        parentClassName: string;
        location: string;
        isSuperclass: boolean;
        ids: any[];
        docEntry: {
            decorators: {
                name: string;
                values: any[];
            }[];
            isGenerated: boolean;
            isId: boolean;
            isMappedSuperclass: boolean;
            isTransient: boolean;
            name: string;
            type: string;
            fileImports: {
                importMapByObjectAsName: {
                    Column: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Column: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            JoinColumn: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            ManyToOne: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            MappedSuperclass: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                            GeneratedValue?: undefined;
                            Id?: undefined;
                        };
                        path: string;
                    };
                    JoinColumn: any;
                    ManyToOne: any;
                    MappedSuperclass: any;
                    IUser: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            IUser: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    Stageable: {
                        fileImports: any;
                        isLocal: boolean;
                        objectMapByAsName: {
                            Stageable: {
                                asName: string;
                                moduleImport: any;
                                sourceName: string;
                            };
                        };
                        path: string;
                    };
                    GeneratedValue?: undefined;
                    Id?: undefined;
                    Actor?: undefined;
                    SystemWideOperationId?: undefined;
                    Repository?: undefined;
                    RepositoryEntity?: undefined;
                };
                importMapByModulePath: {
                    "@airport/air-control": any;
                    "@airport/travel-document-checkpoint": any;
                    "../infrastructure/Stageable": any;
                    "../infrastructure/Actor"?: undefined;
                    "../common"?: undefined;
                    "./Repository"?: undefined;
                    "../repository/RepositoryEntity"?: undefined;
                };
            };
            properties: ({
                decorators: {
                    name: string;
                    values: {
                        name: string;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                fromProject: string;
                otherSchemaDbEntity: {
                    columnMap: any;
                    columns: ({
                        entity: any;
                        id: any;
                        index: number;
                        isGenerated: boolean;
                        manyRelationColumns: any[];
                        name: string;
                        notNull: boolean;
                        oneRelationColumns: any[];
                        propertyColumnMap: any;
                        propertyColumns: {
                            column: any;
                            property: any;
                            sinceVersion: any;
                        }[];
                        sinceVersion: any;
                        type: number;
                        idIndex: number;
                    } | {
                        entity: any;
                        id: any;
                        index: number;
                        isGenerated: boolean;
                        manyRelationColumns: any[];
                        name: string;
                        notNull: boolean;
                        oneRelationColumns: any[];
                        propertyColumnMap: any;
                        propertyColumns: {
                            column: any;
                            property: any;
                            sinceVersion: any;
                        }[];
                        sinceVersion: any;
                        type: number;
                        idIndex?: undefined;
                    })[];
                    idColumns: {
                        entity: any;
                        id: any;
                        index: number;
                        isGenerated: boolean;
                        manyRelationColumns: any[];
                        name: string;
                        notNull: boolean;
                        oneRelationColumns: any[];
                        propertyColumnMap: any;
                        propertyColumns: {
                            column: any;
                            property: any;
                            sinceVersion: any;
                        }[];
                        sinceVersion: any;
                        type: number;
                        idIndex: number;
                    }[];
                    idColumnMap: any;
                    id: any;
                    index: number;
                    isLocal: boolean;
                    isRepositoryEntity: boolean;
                    name: string;
                    propertyMap: any;
                    properties: ({
                        propertyColumns: {
                            column: any;
                            property: any;
                            sinceVersion: any;
                        }[];
                        entity: any;
                        id: any;
                        index: number;
                        isId: boolean;
                        name: string;
                        relation: any;
                        sinceVersion: any;
                    } | {
                        propertyColumns: any[];
                        entity: any;
                        id: any;
                        index: number;
                        isId: boolean;
                        name: string;
                        relation: {
                            isId: boolean;
                            oneToManyElems: any;
                            relationType: number;
                            id: any;
                            index: number;
                            property: any;
                            manyRelationColumns: any[];
                            oneRelationColumns: any[];
                            relationEntity: any;
                            sinceVersion: any;
                        }[];
                        sinceVersion: any;
                    })[];
                    relationReferences: any[];
                    relations: {
                        isId: boolean;
                        oneToManyElems: any;
                        relationType: number;
                        id: any;
                        index: number;
                        property: any;
                        manyRelationColumns: any[];
                        oneRelationColumns: any[];
                        relationEntity: any;
                        sinceVersion: any;
                    }[];
                    schemaVersion: any;
                    sinceVersion: any;
                    tableConfig: any;
                };
                index: number;
                primitive?: undefined;
            } | {
                decorators: {
                    name: string;
                    values: {
                        name: string;
                    }[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                ownerEntity: any;
                nonArrayType: string;
                primitive: string;
                index: number;
                fromProject?: undefined;
                otherSchemaDbEntity?: undefined;
            })[];
            methodSignatures: any[];
            constructors: {
                parameters: any[];
                returnType: string;
            }[];
        };
        implementedInterfaceNames: any[];
        parentEntity: {
            type: string;
            path: string;
            parentClassName: any;
            isSuperclass: boolean;
            ids: any[];
            docEntry: {
                decorators: {
                    name: string;
                    values: any[];
                }[];
                isGenerated: boolean;
                isId: boolean;
                isMappedSuperclass: boolean;
                isTransient: boolean;
                name: string;
                type: string;
                fileImports: {
                    importMapByObjectAsName: {
                        Column: {
                            fileImports: any;
                            isLocal: boolean;
                            objectMapByAsName: {
                                Column: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                MappedSuperclass: {
                                    asName: string;
                                    moduleImport: any;
                                    sourceName: string;
                                };
                                GeneratedValue?: undefined;
                                Id?: undefined;
                                JoinColumn?: undefined;
                                ManyToOne?: undefined;
                            };
                            path: string;
                        };
                        MappedSuperclass: any;
                        GeneratedValue?: undefined;
                        Id?: undefined;
                        JoinColumn?: undefined;
                        ManyToOne?: undefined;
                        Actor?: undefined;
                        SystemWideOperationId?: undefined;
                        Stageable?: undefined;
                        Repository?: undefined;
                    };
                    importMapByModulePath: {
                        "@airport/air-control": any;
                        "../infrastructure/Actor"?: undefined;
                        "../common"?: undefined;
                        "../infrastructure/Stageable"?: undefined;
                        "./Repository"?: undefined;
                    };
                };
                properties: {
                    decorators: {
                        name: string;
                        values: {
                            name: string;
                            nullable: boolean;
                        }[];
                    }[];
                    isGenerated: boolean;
                    isId: boolean;
                    isMappedSuperclass: boolean;
                    isTransient: boolean;
                    name: string;
                    type: string;
                    ownerEntity: any;
                    nonArrayType: string;
                    primitive: string;
                    index: number;
                }[];
                methodSignatures: any[];
                constructors: {
                    parameters: any[];
                    returnType: string;
                }[];
            };
            implementedInterfaceNames: any[];
            project: string;
            location?: undefined;
            parentEntity?: undefined;
        };
        project: string;
    };
    project: string;
} | {
    type: string;
    path: string;
    parentClassName: any;
    isSuperclass: boolean;
    ids: any[];
    docEntry: {
        decorators: {
            name: string;
            values: any[];
        }[];
        isGenerated: boolean;
        isId: boolean;
        isMappedSuperclass: boolean;
        isTransient: boolean;
        name: string;
        type: string;
        fileImports: {
            importMapByObjectAsName: {
                MappedSuperclass: {
                    fileImports: any;
                    isLocal: boolean;
                    objectMapByAsName: {
                        MappedSuperclass: {
                            asName: string;
                            moduleImport: any;
                            sourceName: string;
                        };
                    };
                    path: string;
                };
                Column?: undefined;
                GeneratedValue?: undefined;
                Id?: undefined;
                JoinColumn?: undefined;
                ManyToOne?: undefined;
                Actor?: undefined;
                SystemWideOperationId?: undefined;
                Stageable?: undefined;
                Repository?: undefined;
                RepositoryEntity?: undefined;
                IUser?: undefined;
                ImmutableRepoRow?: undefined;
                ImmutableRow?: undefined;
            };
            importMapByModulePath: {
                "@airport/air-control": any;
                "../infrastructure/Actor"?: undefined;
                "../common"?: undefined;
                "../infrastructure/Stageable"?: undefined;
                "./Repository"?: undefined;
                "../repository/RepositoryEntity"?: undefined;
                "@airport/travel-document-checkpoint"?: undefined;
                "./ImmutableRepoRow"?: undefined;
                "./ImmutableRow"?: undefined;
            };
        };
        properties: any[];
        methodSignatures: any[];
        constructors: {
            parameters: any[];
            returnType: string;
        }[];
    };
    implementedInterfaceNames: any[];
    project: string;
    location?: undefined;
    parentEntity?: undefined;
})[];
//# sourceMappingURL=mappedSuperclass.d.ts.map
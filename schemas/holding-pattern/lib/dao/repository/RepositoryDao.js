import { AIR_DB, and, Y } from '@airport/air-control';
import { container, DI } from '@airport/di';
import { ensureChildJsMap } from '@airport/ground-control';
import { REPOSITORY_DAO } from '../../tokens';
import { BaseRepositoryDao, Q, } from '../../generated/generated';
export class RepositoryDao extends BaseRepositoryDao {
    async findReposWithTransactionLogDetailsByIds(repositoryIds) {
        let r;
        let ra;
        let a;
        let u;
        let d;
        let id = Y;
        return await this.db.find.map().tree({
            select: {
                orderedId: Y,
                randomId: Y,
                ownerActor: {
                    user: {
                        id
                    },
                    terminal: {
                        id
                    }
                },
            },
            from: [
                r = Q.Repository,
                ra = r.repositoryActors.innerJoin(),
                a = ra.actor.innerJoin(),
                u = a.user.innerJoin(),
                d = a.terminal.innerJoin()
            ],
            where: 
            // and(
            r.id.in(repositoryIds),
        });
    }
    async findReposWithDetailsAndSyncNodeIds(repositoryIds) {
        let r;
        const id = Y;
        return await this.db.find.tree({
            select: {
                id,
                ownerActor: {
                    id
                },
                orderedId: Y,
                randomId: Y
            },
            from: [
                r = Q.Repository
            ],
            where: r.id.in(repositoryIds)
        });
    }
    async findReposWithDetailsByIds(repositoryIdsInClause, dbName, userEmail) {
        let r;
        let ra;
        let a;
        let u;
        let d;
        let id = Y;
        return await this.db.find.map().tree({
            select: {
                ...this.db.duo.select.fields,
                repositoryActors: {
                    actor: {
                        user: {
                            id
                        },
                        terminal: {
                            id
                        }
                    }
                },
            },
            from: [
                r = Q.Repository,
                ra = r.repositoryActors.innerJoin(),
                a = ra.actor.innerJoin(),
                u = a.user.innerJoin(),
                d = a.terminal.innerJoin()
            ],
            where: and(r.id.in(repositoryIdsInClause), d.name.equals(dbName), u.uniqueId.equals(userEmail))
        });
    }
    async findReposWithGlobalIds(repositoryIds) {
        const repositoryMapById = new Map();
        let r;
        let a;
        let u;
        const repositories = await this.db.find.tree({
            select: {
                id: Y,
                orderedId: Y,
                randomId: Y,
                ownerActor: {
                    id: Y,
                    user: {
                        uniqueId: Y
                    },
                }
            },
            from: [
                r = Q.Repository,
                a = r.ownerActor.innerJoin(),
                u = a.user.innerJoin()
            ],
            where: r.id.in(repositoryIds)
        });
        for (const repository of repositories) {
            repositoryMapById.set(repository.id, repository);
        }
        return repositoryMapById;
    }
    async findLocalRepoIdsByGlobalIds(orderedIds, randomIds, ownerActorRandomIds, ownerUserUniqueIds, ownerTerminalNames, ownerTerminalSecondIds, ownerTerminalOwnerUserUniqueIds) {
        const repositoryIdMap = new Map();
        let r;
        let oa;
        let od;
        let odu;
        let ou;
        const airDb = await container(this).get(AIR_DB);
        const resultRows = await airDb.find.sheet({
            from: [
                r = Q.Repository,
                oa = r.ownerActor.innerJoin(),
                ou = oa.user.innerJoin(),
                od = oa.terminal.innerJoin(),
                odu = od.owner.innerJoin(),
            ],
            select: [
                odu.uniqueId,
                od.name,
                od.secondId,
                ou.uniqueId,
                oa.randomId,
                r.orderedId,
                r.randomId,
                r.id,
            ],
            where: and(r.orderedId.in(orderedIds), r.randomId.in(randomIds), oa.randomId.in(ownerActorRandomIds), ou.uniqueId.in(ownerUserUniqueIds), od.name.in(ownerTerminalNames), od.secondId.in(ownerTerminalSecondIds), odu.uniqueId.in(ownerTerminalOwnerUserUniqueIds))
        });
        for (const resultRow of resultRows) {
            ensureChildJsMap(ensureChildJsMap(ensureChildJsMap(ensureChildJsMap(ensureChildJsMap(ensureChildJsMap(repositoryIdMap, resultRow[0]), resultRow[1]), resultRow[2]), resultRow[3]), resultRow[4]), resultRow[5]).set(resultRow[6], resultRow[7]);
        }
        return repositoryIdMap;
    }
}
DI.set(REPOSITORY_DAO, RepositoryDao);
//# sourceMappingURL=RepositoryDao.js.map
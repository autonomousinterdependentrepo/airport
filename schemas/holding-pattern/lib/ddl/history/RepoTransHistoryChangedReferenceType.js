export var RepoTransHistoryChangedReferenceType;
(function (RepoTransHistoryChangedReferenceType) {
    RepoTransHistoryChangedReferenceType[RepoTransHistoryChangedReferenceType["ADDED_REFERENCE"] = 0] = "ADDED_REFERENCE";
    RepoTransHistoryChangedReferenceType[RepoTransHistoryChangedReferenceType["REMOVED_REFERENCE"] = 1] = "REMOVED_REFERENCE";
})(RepoTransHistoryChangedReferenceType || (RepoTransHistoryChangedReferenceType = {}));
//# sourceMappingURL=RepoTransHistoryChangedReferenceType.js.map
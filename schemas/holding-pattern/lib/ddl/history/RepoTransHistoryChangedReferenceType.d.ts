export declare enum RepoTransHistoryChangedReferenceType {
    ADDED_REFERENCE = 0,
    REMOVED_REFERENCE = 1
}
//# sourceMappingURL=RepoTransHistoryChangedReferenceType.d.ts.map
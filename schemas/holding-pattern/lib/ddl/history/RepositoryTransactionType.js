export var RepositoryTransactionType;
(function (RepositoryTransactionType) {
    RepositoryTransactionType[RepositoryTransactionType["LOCAL"] = 0] = "LOCAL";
    RepositoryTransactionType[RepositoryTransactionType["REMOTE"] = 1] = "REMOTE";
})(RepositoryTransactionType || (RepositoryTransactionType = {}));
//# sourceMappingURL=RepositoryTransactionType.js.map
import { ImmutableRepoRow } from './ImmutableRepoRow';
export declare class MutableRepoRow extends ImmutableRepoRow {
    updatedAt: Date;
}
//# sourceMappingURL=MutableRepoRow.d.ts.map
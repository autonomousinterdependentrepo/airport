import { RepositoryEntity } from '../repository/RepositoryEntity';
export declare abstract class ImmutableRepoRow extends RepositoryEntity {
    createdAt: Date;
}
//# sourceMappingURL=ImmutableRepoRow.d.ts.map
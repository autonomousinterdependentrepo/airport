import { IUser } from '@airport/travel-document-checkpoint';
import { Stageable } from '../infrastructure/Stageable';
export declare abstract class ImmutableRow extends Stageable {
    user: IUser;
    createdAt: Date;
}
//# sourceMappingURL=ImmutableRow.d.ts.map
import { SchemaIndex } from "@airport/ground-control";
import { Repository } from './Repository';
export declare type RepositorySchemaId = number;
export declare class RepositorySchema {
    id: RepositorySchemaId;
    repository: Repository;
    schemaIndex: SchemaIndex;
}
//# sourceMappingURL=RepositorySchema.d.ts.map
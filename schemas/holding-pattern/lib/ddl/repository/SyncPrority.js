export var SyncPriority;
(function (SyncPriority) {
    SyncPriority[SyncPriority["VITAL"] = 0] = "VITAL";
    SyncPriority[SyncPriority["CRITICAL"] = 1] = "CRITICAL";
    SyncPriority[SyncPriority["HIGH"] = 2] = "HIGH";
    SyncPriority[SyncPriority["ABOVE_NORMAL"] = 3] = "ABOVE_NORMAL";
    SyncPriority[SyncPriority["NORMAL"] = 4] = "NORMAL";
    SyncPriority[SyncPriority["BELOW_NORMAL"] = 5] = "BELOW_NORMAL";
    SyncPriority[SyncPriority["LOW"] = 6] = "LOW";
})(SyncPriority || (SyncPriority = {}));
//# sourceMappingURL=SyncPrority.js.map
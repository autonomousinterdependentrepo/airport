import { Application } from '../infrastructure/Application';
import { Repository } from './Repository';
/**
 * Created by Papa on 12/18/2016.
 */
/**
 * A record of device+datatabase that adds to a repository
 */
export declare class RepositoryApplication {
    id: number;
    application: Application;
    repository: Repository;
}
//# sourceMappingURL=RepositoryApplication.d.ts.map
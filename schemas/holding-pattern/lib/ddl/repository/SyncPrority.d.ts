export declare enum SyncPriority {
    VITAL = 0,
    CRITICAL = 1,
    HIGH = 2,
    ABOVE_NORMAL = 3,
    NORMAL = 4,
    BELOW_NORMAL = 5,
    LOW = 6
}
//# sourceMappingURL=SyncPrority.d.ts.map
export declare type Stageable_Draft = boolean;
export declare abstract class Stageable {
    draft: Stageable_Draft;
}
//# sourceMappingURL=Stageable.d.ts.map
import { Actor } from './Actor';
import { Application } from './Application';
/**
 * Created by Papa on 12/18/2016.
 */
/**
 * A record of device+datatabase that adds to a repository
 */
export declare class ActorApplication {
    id: number;
    actor: Actor;
    application: Application;
}
//# sourceMappingURL=ActorApplication.d.ts.map
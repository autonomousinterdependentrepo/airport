"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("@airport/di");
exports.ACTOR_DAO = di_1.diToken();
exports.OPER_HISTORY_DUO = di_1.diToken();
exports.REC_HISTORY_DUO = di_1.diToken();
exports.REC_HIST_NEW_VALUE_DAO = di_1.diToken();
exports.REC_HIST_NEW_VALUE_DUO = di_1.diToken();
exports.REC_HIST_OLD_VALUE_DAO = di_1.diToken();
exports.REC_HIST_OLD_VALUE_DUO = di_1.diToken();
exports.REPO_ACTOR_DAO = di_1.diToken();
exports.REPOSITORY_DAO = di_1.diToken();
exports.REPO_TRANS_HISTORY_DAO = di_1.diToken();
exports.REPO_TRANS_HISTORY_DUO = di_1.diToken();
exports.TRANS_HISTORY_DUO = di_1.diToken();
//# sourceMappingURL=diTokens.js.map
import {
	IStageable,
} from '../infrastructure/stageable';



//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IChildRow extends IStageable {
	
	// Id Properties

	// Id Relations

	// Non-Id Properties

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



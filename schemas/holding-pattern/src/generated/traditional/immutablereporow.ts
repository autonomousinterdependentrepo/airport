import {
	IRepositoryEntity,
} from '../repository/repositoryentity';



//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IImmutableRepoRow extends IRepositoryEntity {
	
	// Id Properties

	// Id Relations

	// Non-Id Properties
	createdAt?: Date;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



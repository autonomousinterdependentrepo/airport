import {
	IImmutableRepoRow,
} from './immutablereporow';



//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IMutableRepoRow extends IImmutableRepoRow {
	
	// Id Properties

	// Id Relations

	// Non-Id Properties
	updatedAt?: Date;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



import {
	IRepositoryEntity,
} from '../repository/repositoryentity';



//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IChildRepoRow extends IRepositoryEntity {
	
	// Id Properties

	// Id Relations

	// Non-Id Properties

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



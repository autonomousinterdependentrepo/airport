


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface IStageable {
	
	// Id Properties

	// Id Relations

	// Non-Id Properties
	draft?: boolean;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



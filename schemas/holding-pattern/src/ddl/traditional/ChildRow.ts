import {MappedSuperclass} from '@airport/air-control'
import {Stageable}        from '../infrastructure/Stageable'

@MappedSuperclass()
export abstract class ChildRow
	extends Stageable {

}

export enum RepoTransHistoryChangedReferenceType {
	ADDED_REFERENCE,
	REMOVED_REFERENCE
}
import { IUser } from './user';
import { ITerminal } from './terminal';
export interface IUserTerminal {
    user: IUser;
    terminal: ITerminal;
}
//# sourceMappingURL=userterminal.d.ts.map
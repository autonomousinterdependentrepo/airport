import { ITerminalDao } from './dao/TerminalDao';
import { IUserDao } from './dao/UserDao';
export declare const TERMINAL_DAO: import("@airport/di").IDiToken<ITerminalDao>;
export declare const USER_DAO: import("@airport/di").IDiToken<IUserDao>;
//# sourceMappingURL=tokens.d.ts.map
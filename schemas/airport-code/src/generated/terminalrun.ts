


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ITerminalRun {
	
	// Id Properties
	id: number;

	// Id Relations

	// Non-Id Properties
	createTimestamp?: number;
	randomNumber?: number;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



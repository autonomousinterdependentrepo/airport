


//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ISystemWideOperationId {
	
	// Id Properties
	id: number;

	// Id Relations

	// Non-Id Properties

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}






//////////////////////////////
//     ENTITY INTERFACE     //
//////////////////////////////

export interface ISequence {
	
	// Id Properties
	schemaIndex: number;
	tableIndex: number;
	columnIndex: number;

	// Id Relations

	// Non-Id Properties
	incrementBy?: number;
	currentValue?: number;

	// Non-Id Relations

	// Transient Properties

	// Public Methods
	
}



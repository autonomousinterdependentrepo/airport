export declare type TerminalRunId = number;
export declare type TerminalRunCreateTimestamp = number;
export declare type TerminalRunRandomNumber = number;
/**
 * A record of the Terminal running (being up at a given point in time)
 */
export declare class TerminalRun {
    id: TerminalRunId;
    createTimestamp: TerminalRunCreateTimestamp;
    randomNumber: TerminalRunRandomNumber;
}
//# sourceMappingURL=terminalrun.d.ts.map
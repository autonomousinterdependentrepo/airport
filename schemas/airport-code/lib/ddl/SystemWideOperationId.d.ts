export declare type SystemWideOperationId_Id = number;
/**
 * No actual records are inserted into this table, only used for the sequence
 */
export declare class SystemWideOperationId {
    id: SystemWideOperationId_Id;
}
//# sourceMappingURL=systemwideoperationid.d.ts.map
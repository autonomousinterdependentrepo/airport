import { ISequenceDao } from './dao/SequenceDao';
import { ITerminalRunDao } from './dao/TerminalRunDao';
export declare const SEQUENCE_DAO: import("@airport/di").IDiToken<ISequenceDao>;
export declare const TERMINAL_RUN_DAO: import("@airport/di").IDiToken<ITerminalRunDao>;
//# sourceMappingURL=tokens.d.ts.map
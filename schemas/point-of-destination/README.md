# Point of Destination

This is the point of destination for all archived records.  Used in setups where it is preferrable
to store the data in the database, instead of on cloud accounts as one file per repository per 
day.

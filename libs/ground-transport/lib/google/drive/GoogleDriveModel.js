/**
 * Created by Papa on 1/10/2016.
 */
export class MimeTypes {
}
MimeTypes.FOLDER = 'application/vnd.google-apps.folder';
MimeTypes.REALTIME = 'application/vnd.google-apps.drive-sdk';
MimeTypes.SPREAD_SHEET_BOOK = 'application/vnd.google-apps.spreadsheet';
export class DriveConstants {
}
DriveConstants.APP_DATA_FOLDER = 'appDataFolder';
DriveConstants.DRIVE_FOLDER = 'root';
DriveConstants.DRIVE_SPACE = 'drive';
DriveConstants.APP_DATA_LIST_FIELDS = 'nextPageToken, files(id, name)';
DriveConstants.VERSION = 'v3';
//# sourceMappingURL=GoogleDriveModel.js.map
import { DI } from '@airport/di';
import { MISSING_RECORD_CREATOR } from '../../../tokens';
export class MissingRecordCreator {
}
DI.set(MISSING_RECORD_CREATOR, MissingRecordCreator);
//# sourceMappingURL=MissingRecordCreator.js.map
import {ILoggedPackage, LoggedPackage} from "./LoggedPackage";

export const APPROACH_LIGHTING_SYSTEM_LOGGER: ILoggedPackage
	= new LoggedPackage("approach-lighting-system");
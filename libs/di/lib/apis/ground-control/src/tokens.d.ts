import { IStoreDriver } from './lingo/data/StoreDriver';
import { ITransactionalConnector } from './lingo/ITransactionalConnector';
import { IRxJs } from './lingo/IRxJs';
export declare const CONFIG: any;
export declare const RXJS: import("@airport/di").IDiToken<IRxJs>;
export declare const STORE_DRIVER: import("@airport/di").IDiToken<IStoreDriver>;
export declare const TRANS_CONNECTOR: import("@airport/di").IDiToken<ITransactionalConnector>;
//# sourceMappingURL=tokens.d.ts.map
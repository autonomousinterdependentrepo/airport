export declare enum BlockSyncStatus {
    SYNCHRONIZING = 0,
    SYNCHRONIZED = 1,
    REQUESTING_SYNC_STATUS = 2,
    RESYNC_REQUESTED = 3
}
export declare enum RepositorySyncStatus {
    ACTIVE = 0,
    PENDING = 1,
    DELAYED = 2,
    SUSPENDED = 3,
    TEMPORARILY_REROUTED = 4,
    PERMANENTLY_REROUTED = 5
}
export declare enum TerminalSyncStatus {
    ACTIVE = 0,
    SUSPENDED = 1
}
//# sourceMappingURL=SyncStatus.d.ts.map
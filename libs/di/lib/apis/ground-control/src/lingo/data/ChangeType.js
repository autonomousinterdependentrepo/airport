export var ChangeType;
(function (ChangeType) {
    ChangeType[ChangeType["INSERT_VALUES"] = 0] = "INSERT_VALUES";
    ChangeType[ChangeType["DELETE_ROWS"] = 1] = "DELETE_ROWS";
    ChangeType[ChangeType["UPDATE_ROWS"] = 2] = "UPDATE_ROWS";
})(ChangeType || (ChangeType = {}));
//# sourceMappingURL=ChangeType.js.map
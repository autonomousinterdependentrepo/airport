export var QueryType;
(function (QueryType) {
    QueryType[QueryType["DDL"] = 0] = "DDL";
    QueryType[QueryType["SELECT"] = 1] = "SELECT";
    QueryType[QueryType["MUTATE"] = 2] = "MUTATE";
})(QueryType || (QueryType = {}));
//# sourceMappingURL=IStoreOperator.js.map
export var SchemaStatus;
(function (SchemaStatus) {
    SchemaStatus[SchemaStatus["CURRENT"] = 0] = "CURRENT";
    SchemaStatus[SchemaStatus["MISSING"] = 1] = "MISSING";
    SchemaStatus[SchemaStatus["NEEDS_UPGRADES"] = 2] = "NEEDS_UPGRADES";
})(SchemaStatus || (SchemaStatus = {}));
//# sourceMappingURL=SchemaStatus.js.map
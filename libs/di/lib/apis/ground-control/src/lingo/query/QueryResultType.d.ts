export declare enum QueryResultType {
    ENTITY_GRAPH = 0,
    ENTITY_TREE = 1,
    TREE = 2,
    SHEET = 3,
    FIELD = 4,
    RAW = 5,
    MAPPED_ENTITY_GRAPH = 6,
    MAPPED_ENTITY_TREE = 7
}
//# sourceMappingURL=QueryResultType.d.ts.map
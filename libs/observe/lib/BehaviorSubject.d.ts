import type { BehaviorSubject as IRxBehaviorSubject } from 'rxjs';
export interface IBehaviorSubject<V> extends IRxBehaviorSubject<V> {
}
//# sourceMappingURL=BehaviorSubject.d.ts.map
/**
 * A SubscriptionObserver is a normalized Observer which wraps the observer object
 * supplied to subscribe.
 */
// export interface SubscriptionObserver<V> {
//
// 	// Sends the next value in the sequence
// 	next(value: V): void;
//
// 	// Sends the sequence error
// 	error(errorValue): void;
//
// 	// Sends the completion notification
// 	complete(): void;
//
// 	// A boolean value indicating whether the subscription is closed
// 	closed: boolean;
//
// }
//# sourceMappingURL=SubscriptionObserver.js.map
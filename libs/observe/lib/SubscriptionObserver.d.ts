/**
 * A SubscriptionObserver is a normalized Observer which wraps the observer object
 * supplied to subscribe.
 */
//# sourceMappingURL=SubscriptionObserver.d.ts.map
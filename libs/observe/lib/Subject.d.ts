import type { Subject as IRxSubject } from 'rxjs';
export interface ISubject<V> extends IRxSubject<V> {
}
//# sourceMappingURL=Subject.d.ts.map
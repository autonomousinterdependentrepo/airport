# Tower

The tower is a tall, windowed structure located on the airport grounds.
The primary method of controlling the immediate airport environment is
visual observation from the airport control tower.
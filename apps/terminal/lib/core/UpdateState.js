export var UpdateState;
(function (UpdateState) {
    UpdateState[UpdateState["GO_ONLINE"] = 0] = "GO_ONLINE";
    UpdateState[UpdateState["REMOTE"] = 1] = "REMOTE";
    UpdateState[UpdateState["LOCAL"] = 2] = "LOCAL";
})(UpdateState || (UpdateState = {}));
//# sourceMappingURL=UpdateState.js.map
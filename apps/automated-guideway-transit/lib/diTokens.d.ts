import { ILoggedPackage } from '@airport/approach-lighting-system';
export declare const TUNNING_SETTINGS: any;
export declare const ERROR_LOGGER: any;
export declare const SYNC_CONNECTION_PROCESSOR: any;
export declare const SYNC_CONNECTION_VERIFIER: any;
export declare const BLACKLIST: any;
export declare const AGTLogger: ILoggedPackage;

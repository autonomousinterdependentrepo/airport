"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ClientInOperation;
(function (ClientInOperation) {
    ClientInOperation[ClientInOperation["CONNECT"] = 0] = "CONNECT";
    ClientInOperation[ClientInOperation["ADD_DATA"] = 1] = "ADD_DATA";
})(ClientInOperation = exports.ClientInOperation || (exports.ClientInOperation = {}));
//# sourceMappingURL=ClientMessage.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OutClientMessageType;
(function (OutClientMessageType) {
    OutClientMessageType[OutClientMessageType["SYNC_RECORD"] = 0] = "SYNC_RECORD";
    OutClientMessageType[OutClientMessageType["DATABASE_SYNC_LOG"] = 1] = "DATABASE_SYNC_LOG";
})(OutClientMessageType = exports.OutClientMessageType || (exports.OutClientMessageType = {}));
//# sourceMappingURL=ClientMessageType.js.map
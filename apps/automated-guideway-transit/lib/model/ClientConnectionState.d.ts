export declare enum ClientConnectionState {
    PENDING = 1,
    UNVERIFIED = 2,
    ACTIVE = 3,
    INACTIVE = 4
}
//# sourceMappingURL=ClientConnectionState.d.ts.map
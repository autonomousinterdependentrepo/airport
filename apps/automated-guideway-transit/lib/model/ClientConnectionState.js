export var ClientConnectionState;
(function (ClientConnectionState) {
    ClientConnectionState[ClientConnectionState["PENDING"] = 1] = "PENDING";
    ClientConnectionState[ClientConnectionState["UNVERIFIED"] = 2] = "UNVERIFIED";
    ClientConnectionState[ClientConnectionState["ACTIVE"] = 3] = "ACTIVE";
    ClientConnectionState[ClientConnectionState["INACTIVE"] = 4] = "INACTIVE";
})(ClientConnectionState || (ClientConnectionState = {}));
//# sourceMappingURL=ClientConnectionState.js.map
export declare enum OutClientMessageType {
    SYNC_RECORD = 0,
    DATABASE_SYNC_LOG = 1,
}

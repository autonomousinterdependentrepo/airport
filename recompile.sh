cd libs/di
tsc
cd ../observe
tsc
cd ../../apis/ground-control
tsc
cd ../air-control
tsc
cd ../arrivals-n-departures
tsc
cd ../../libs/tower
tsc
cd ../../apis/check-in
tsc
cd ../../generators/runway
tsc
cd ../../schemas/territory
tsc
cd ../airport-code
tsc
cd ../flight-log-archive
tsc
cd ../guideway
tsc
cd ../traffic-pattern
tsc
cd ../travel-document-checkpoint
tsc
cd ../holding-pattern
tsc
cd ../moving-walkway
tsc
cd ../point-of-destination
tsc
cd ../runway-edge-lighting
tsc
cd ../../libs/approach-lighting-system
tsc
cd ../blueprint
tsc
cd ../../apis/terminal-map
tsc
cd ../../libs/fuel-hydrant-system
tsc
cd ../ground-transport
tsc
cd ../mono-rail
tsc
cd ../../apps/terminal
tsc
cd ../tarmaq
tsc
cd ../automated-guideway-transit
tsc
cd ../..

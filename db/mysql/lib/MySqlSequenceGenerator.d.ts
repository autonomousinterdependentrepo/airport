import { SequenceGenerator } from '@airport/sequence';
export declare class MySqlSequenceGenerator extends SequenceGenerator {
    protected nativeGenerate(): Promise<number>;
}
//# sourceMappingURL=MySqlSequenceGenerator.d.ts.map